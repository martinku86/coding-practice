package tree;

import java.util.LinkedList;
import java.util.List;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
}

public class BinaryTreeSumPath {

	//Given a binary tree of integers, and given an integer. Print all paths that sums up to the int
	public void printPathSum() {
		
	}
	
	public List<List<Integer>> pathSum(TreeNode root, int sum) {
        // in-order traversal
        List<List<Integer>> pathList = new LinkedList<List<Integer>>();
        LinkedList<Integer> path = new LinkedList<Integer>();
        return pathSumRecursive(root, 0, 0, pathList, path);
    }
    
    public List<List<Integer>> pathSumRecursive(TreeNode currNode, int sum, int runningSum, List<List<Integer>> pathList, LinkedList<Integer> curPath) {
        int curRunningSum = runningSum + currNode.val;
        curPath.add(currNode.val);
        
        if (currNode.left == null && currNode.right == null) {
            if (curRunningSum == sum)
                pathList.add((List<Integer>)curPath.clone());
        }
        
        if (currNode.left != null) {
            pathSumRecursive(currNode.left, sum, curRunningSum, pathList, curPath);
        }
        if (currNode.right != null) {
            pathSumRecursive(currNode.right, sum, curRunningSum, pathList, curPath);
        }

        curPath.removeLast();
        return pathList;
    }
}
