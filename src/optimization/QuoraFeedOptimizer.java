package optimization;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;

public class QuoraFeedOptimizer {

	static int storyId = 1;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		// number of events
		int N = sc.nextInt();
		// window for the number of recent stories to consider
		int W = sc.nextInt();
		// pixel available to use
		int R = sc.nextInt();
		
		LinkedList<Event> window = new LinkedList<Event>();
		for (int i = 0; i < N; i++) {
			String line = sc.nextLine();
			Event event = new Event(line);
			if (event.isReload) {
				EventKnapSack solutionSack = solveEventKnapSack(window, R);
				String resultString = convertEventsToString(solutionSack);
				System.out.println(resultString);
			}
			else {
				addToWindow(window, W, event);
			}
		}
	}
	
	private static String convertEventsToString(EventKnapSack sack) {
		StringBuilder builder = new StringBuilder();
		Iterator<Event> iter = sack.events.iterator();
		while (iter.hasNext()) {
			builder.append(iter.next().id);
			if (iter.hasNext())
				builder.append(" ");
		}
		return String.format("%d %d %s", sack.score, sack.events.size(), builder.toString());
	}
	
	private static EventKnapSack solveEventKnapSack(List<Event> window, int R) {
		int highestScore = 0;
		int highestScoreWeight = 0;
		
		Map<Integer, PriorityQueue<EventKnapSack>> sackMap = new HashMap<Integer, PriorityQueue<EventKnapSack>>();
		
		// add all single event as knapsacks
		for (Event event : window) {
			EventKnapSack sack = new EventKnapSack(event);
			if (!sackMap.containsKey(sack.pixels)) {
				sackMap.put(sack.pixels, new PriorityQueue<EventKnapSack>(new ScoreComparator()));
			}
			sackMap.get(sack.pixels).offer(sack);
		}
		
		// iterate from 1 pixel to R, find optimal knapsack for each weight. Update highestScore
		for (int i = 1; i <= R; i++) {
			int iHighScore = 0;
			int iHighScoreW = 0;
			for (int j = i; j > (i / 2); j--) {
				if (!sackMap.containsKey(j))
					continue;
				
				PriorityQueue<EventKnapSack> jSackQ = sackMap.get(j);
				int o = j - i;
				if (o == 0) {
					if (iHighScore > jSackQ.peek().score) {
						iHighScore = jSackQ.peek().score;
						iHighScoreW = j;
					}
					continue;
				}
				
				if (!sackMap.containsKey(o))
					continue;
				
				PriorityQueue<EventKnapSack> oSackQ = sackMap.get(o);
				int jScore = jSackQ.peek().score;
				int oScore = oSackQ.peek().score;
				
				if (jScore + oScore > iHighScore) {
					iHighScore = jScore + oScore;
					iHighScoreW = j;
				}
			}
			
			if (iHighScore == 0)
				continue;
			
			EventKnapSack sack = sackMap.get(iHighScoreW).poll();
			int opposeW = i - iHighScoreW;
			if (opposeW > 0) {
				sack = sackMap.get(opposeW).poll().merge(sack);
			}
			if (!sackMap.containsKey(i))
				sackMap.put(i, new PriorityQueue<EventKnapSack>(new ScoreComparator()));
			sackMap.get(i).offer(sack);
			
			if (iHighScore > highestScore) {
				highestScore = iHighScore;
				highestScoreWeight = iHighScoreW;
			}
		}
		
		return sackMap.get(highestScoreWeight).peek();
	}
	
	private static void addToWindow(LinkedList<Event> window, int W, Event event) {
		window.addFirst(event);
		if (window.size() > W) {
			window.removeLast();
		}
	}
	
	static class EventKnapSack {
		int pixels;
		int score;
		LinkedList<Event> events;
		
		public EventKnapSack(Event event) {
			this.pixels = event.pixels;
			this.score = event.score;
			events = new LinkedList<Event>();
			events.add(event);
		}
		
		public EventKnapSack merge(EventKnapSack other) {
			this.pixels = this.pixels + other.pixels;
			this.score = this.score + other.score;
			this.events.addAll(other.events);
			return this;
		}
	}
	
	static class Event {
		boolean isReload;
		int time;
		int score;
		int pixels;
		int id;
		
		public Event(String s) {
			String[] mdata = s.split(" ");
			if (mdata[0].equals("R")) {
				isReload = true;
				time = Integer.valueOf(mdata[1]);
			}
			else {
				isReload = false;
				time = Integer.valueOf(mdata[1]);
				score = Integer.valueOf(mdata[2]);
				pixels = Integer.valueOf(mdata[3]);
				id = storyId;
				storyId++;
			}
		}
	}
	
	static class ScoreComparator implements Comparator<EventKnapSack> {
		@Override
		public int compare(EventKnapSack arg0, EventKnapSack arg1) {
			return -(arg0.score - arg1.score);
		}
	}
}
