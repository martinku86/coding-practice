package optimization;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Original problem - https://leetcode.com/problems/4sum/
 * Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.
 * Note: The solution set must not contain duplicate quadruplets
 * 
 * @author Martin
 *
 */
public class FourSum {
    public static final int MIN_ARRAY_SIZE = 4;
    public List<List<Integer>> fourSum(int[] nums, int target) {
        Map<Integer, Map<Integer, List<Pair>>> pairsBySumMap = new HashMap<Integer, Map<Integer, List<Pair>>>();

        Map<Integer, Set<Pair>> dsetByIndexMap = new HashMap<Integer, Set<Pair>>();
        
        for (int i = 0; i < nums.length-1; i++) {
            for (int j = i+1; j < nums.length; j++) {
                Pair pair = new Pair(nums, i, j);
                if (!pairsBySumMap.containsKey(pair.getSum())) {
                    pairsBySumMap.put(pair.getSum(), new HashMap<Integer, List<Pair>>());
                }

                Map<Integer, List<Pair>> setByIndexes = pairsBySumMap.get(pair.getSum());
                //add by the first index -> we can use this for filtering
                int primaryIndex = pair.getIndexes()[0];
                if (!setByIndexes.containsKey(primaryIndex)) {
                    setByIndexes.put(primaryIndex, new LinkedList<Pair>());
                }
                setByIndexes.get(primaryIndex).add(pair);

                //add to repsective disjoint set
                registerIndex(dsetByIndexMap, pair);
            }
        }
        
        // TODO should be possible to further reduce branches by sorting the sum, and perform shrinking window search to match pairs
        Set<Integer> doneSum = new HashSet<Integer>();
        List<List<Integer>> resultList = new LinkedList<List<Integer>>();
        for (Map.Entry<Integer, Map<Integer, List<Pair>>> sumAndDsetMapEntry : pairsBySumMap.entrySet()) {
            // check if sum is done
            Integer currentPairSum = sumAndDsetMapEntry.getKey();
            if (doneSum.contains(currentPairSum))
                continue;

            Integer opposingSum = target - currentPairSum;
            Set<Pair> donePair = new HashSet<Pair>();
            if (pairsBySumMap.containsKey(opposingSum)) {
                // get all sets of pairs that adds to opposingSum, mapped by their Pair Indexes
                Map<Integer, List<Pair>> opposingPairsByIndex = pairsBySumMap.get(opposingSum);

                // loop through current set of sum to get candidate pairs
                Map<Integer, List<Pair>> currentPairByIndex = sumAndDsetMapEntry.getValue();
                for (Map.Entry<Integer, List<Pair>> PairByIndexEntry : currentPairByIndex.entrySet()) {
                    for (Pair currentPair : PairByIndexEntry.getValue()) {
                        if (donePair.contains(currentPair))
                            continue;
                        
                        for (Map.Entry<Integer, List<Pair>> opposingPairIndexEntry : opposingPairsByIndex.entrySet()) {
                            // check if opposing sum's index clashes with currentPair's index
                            // this only checks the primary (first) index, another check pairsDisjoint required
                            if (isCurrentPairIndex(opposingPairIndexEntry.getKey(), currentPair))
                                continue;

                            for (Pair opposedPair : opposingPairIndexEntry.getValue()) {
                                // check if currentPair's index overlaps opposedPair by finding their disjoint set
                                if (!pairsDisjoint(dsetByIndexMap, currentPair, opposedPair)) {
                                    Integer result[] = compilePairsToResult(nums, currentPair, opposedPair);
                                    if (!combinationExists(result)) {
                                        resultList.add(Arrays.asList(result));
                                        registerCombination(result);
                                    }
                                }

                                // skip pairs we already evaluated - should only happen if currentPair.getSum() == opposedPair.getSum()
                                donePair.add(opposedPair);
                            }
                        }
                        
                        donePair.add(currentPair);
                    }
                }
                
                doneSum.add(opposingSum);
            }
            doneSum.add(currentPairSum);
        }
        return resultList;
    }

    private Integer[] compilePairsToResult(int[] nums, Pair pair, Pair other) {
    	Integer[] indexes = new Integer[pair.getIndexes().length + other.getIndexes().length];
        for (int i = 0; i < indexes.length; i++) {
            if (i < pair.getIndexes().length) {
                indexes[i] = nums[pair.getIndexes()[i]];
            }
            else {
                indexes[i] = nums[other.getIndexes()[i - pair.getIndexes().length]];
            }
        }

        Arrays.sort(indexes);
        return indexes;
    }

    HashSet<Integer> combinationHashSet;
    private void registerCombination(Integer[] combinationIndexes) {
        if (combinationHashSet == null)
            combinationHashSet = new HashSet<Integer>();
        combinationHashSet.add(Arrays.hashCode(combinationIndexes));
    }

    private boolean combinationExists(Integer[] combinationIndexes) {
        if (combinationHashSet == null)
            return false;
        
        return combinationHashSet.contains(Arrays.hashCode(combinationIndexes));
    }

    private boolean pairsDisjoint(Map<Integer, Set<Pair>> dSetMap, Pair currentPair, Pair opposedPair) {
        for (int currentPairIndex : currentPair.getIndexes()) {
            Set<Pair> dset = dSetMap.get(currentPairIndex);
            if (dset.contains(opposedPair)) {
                return true;
            }
        }
        return false;
    }

    private boolean isCurrentPairIndex(int opposingIndex, Pair currentPair) {
        for (int currentPairIndex : currentPair.getIndexes()) {
            if (currentPairIndex == opposingIndex)
                return true;
        }
        return false;
    }

    private void registerIndex(Map<Integer, Set<Pair>> dsetByIndex, Pair pair) {
        for (int index : pair.getIndexes()) {
            if (!dsetByIndex.containsKey(index))
                dsetByIndex.put(index, new HashSet<Pair>());
            dsetByIndex.get(index).add(pair);
        }
    }
    
    class Pair {
        int[] nums;
        int[] indexes;
        
        public Pair(int[] nums, int indexA, int indexB) {
            this.nums = nums;
            this.indexes = new int[]{indexA, indexB};
        }
        
        public int getSum() {
            return nums[indexes[0]] + nums[indexes[1]];
        }
        
        public int[] getIndexes() {
            return indexes;
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(indexes);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            if (!(obj instanceof Pair))
                return false;
            
            Pair other = (Pair) obj;
            return Arrays.equals(this.getIndexes(), other.getIndexes());
        }
    }
}