package optimization;

import java.util.Map;
import java.util.TreeMap;

/**
 * Original career cup question - https://www.careercup.com/question?id=5733237978562560
 * @author Martin
 *
 */
public class EffecientCalculatePower {
	public static int power (int n, int x) {
		if (x == 0)
			return 1;
		TreeMap<Integer, Integer> powerMap = mapPowerBySquare(n, x);
		return calculatePowerDynamic(x, powerMap);
	}

	private static int calculatePowerDynamic(int x, TreeMap<Integer, Integer> powersMap) {
		int result = 1;
		
		while (x != 0) {
			Map.Entry<Integer, Integer> powEntry = powersMap.floorEntry(x);
			result = powEntry.getValue() * result;
			x = x - powEntry.getKey();
		}
		
		return result;
	}

	private static TreeMap<Integer, Integer> mapPowerBySquare(int n, int x) {
		TreeMap<Integer, Integer> powerMap = new TreeMap<Integer, Integer>();
		int factor = 1;

		while (factor <= x) {
			powerMap.put(factor, n);
			n = n * n;
			factor = factor * 2;
		}
		return powerMap;
	}

}
