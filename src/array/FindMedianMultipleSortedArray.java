package array;

public class FindMedianMultipleSortedArray {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        SearchWindow window1 = new SearchWindow(nums1);
        SearchWindow window2 = new SearchWindow(nums2);
        
        int medianIndex = ((nums1.length + nums2.length - 1) / 2);
        int searchedIndex = 0;
        
        while (searchedIndex < medianIndex) {
            SearchWindow currWindow = null;
            if (!window2.hasRemaining() || (window1.hasRemaining() && (
                (window1.midValue() < window2.midValue()) || 
                (window1.midValue() == window2.midValue() && window1.reduceWindowLeftSize() >= window2.reduceWindowLeftSize())
                )))
                currWindow = window1;
            else
                currWindow = window2;
            
            searchedIndex += searchForMedian(currWindow, searchedIndex, medianIndex);
        }
        
        SearchWindow candidateWindow = null;
        SearchWindow otherWindow = null;
        
        if (!window2.hasRemaining() || (window1.hasRemaining() && window1.getLeft() <= window2.getLeft())) {
            candidateWindow = window1;
            otherWindow = window2;
        }
        else {
            candidateWindow = window2;
            otherWindow = window1;
        }
        
        double firstMedian = (double) candidateWindow.getLeft();
        candidateWindow.advanceLeft();
        if ((nums1.length + nums2.length) % 2 == 1)
            return firstMedian;
        
        double secondMedian = 0;
        if  (!otherWindow.hasRemaining() || (candidateWindow.hasRemaining() && candidateWindow.getLeft() <= otherWindow.getLeft()))
            secondMedian = (double) candidateWindow.getLeft();
        else
            secondMedian = (double) otherWindow.getLeft();
        return (firstMedian + secondMedian) / 2;
    }

    private int searchForMedian(SearchWindow window, int searchedIndex, int medianIndex) {
        if (window.reduceWindowLeftSize() + searchedIndex > medianIndex) {
            window.reduceWindowRight();
            return 0;
        }
        return window.reduceWindowLeft();
    }
    
    class SearchWindow {
        int[] array;
        int left;
        int right;

        public SearchWindow(int[] array) {
            this.array = array;
            left = 0;
            right = array.length;
        }
        
        public int midIndex() {
            return ((right - left - 1) / 2) + left;
        }
        
        public int midValue() {
            return array[midIndex()];
        }
        
        //reduce right not including midIndex
        public int reduceWindowRight() {
            int reducedSize = right - midIndex() - 1;
            right = midIndex() + 1;
            return reducedSize;
        }
        
        public int reduceWindowLeftSize() {
            return midIndex() - left + 1;
        }
        
        //reduce left including midIndex
        public int reduceWindowLeft() {
            int reducedSize = reduceWindowLeftSize();
            left = midIndex() + 1;
            return reducedSize;
        }
        
        public boolean hasRemaining() {
            return left < right;
        }
        
        public void advanceLeft() {
            left++;
        }
        
        public int getLeft() {
            return array[left];
        }
    }
}
