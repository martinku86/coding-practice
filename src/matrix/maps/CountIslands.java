package matrix.maps;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import concepts.Coordinate;

public class CountIslands {

	public static int countIslands(int[][] map) {
		boolean[][] exploredAreaMap = new boolean[map.length][];
		for (int i = 0; i < map.length; i++)
			exploredAreaMap[i] = new boolean[map[i].length];
		
		Queue<Coordinate> traversalQ = new LinkedList<Coordinate>();
		traversalQ.add(new Coordinate(0,0));
		
		int landMassCount = 0;
		// traverse map
		while (!traversalQ.isEmpty()) {
			Coordinate currentLoc = traversalQ.poll();
			// if explored, ignore
			if (isExplored(exploredAreaMap, currentLoc)) {
				continue;
			}
			
			// encounter unknown landmass, fully map it
			if (isLandMass(map, currentLoc)) {
				landMassCount++;
				fullyMapCurrentLandMass(map, exploredAreaMap, currentLoc, traversalQ);
			}
			else {
				traversalQ.addAll(getUnexploredNeighbors(currentLoc, exploredAreaMap));
			}
			exploredAreaMap[currentLoc.getRow()][currentLoc.getColumn()] = true;
		}
		return landMassCount++;
	}
	
	private static void fullyMapCurrentLandMass(int[][] map, boolean[][] exploredAreaMap, Coordinate currentLoc, Queue<Coordinate> pathQ) {
		Queue<Coordinate> exploreLandQ = new LinkedList<Coordinate>();
		exploreLandQ.add(currentLoc);
		
		while(!exploreLandQ.isEmpty()) {
			currentLoc = exploreLandQ.poll();
			if (isExplored(exploredAreaMap, currentLoc)) {
				continue;
			}
			exploredAreaMap[currentLoc.getRow()][currentLoc.getColumn()] = true;
			
			List<Coordinate> neighbors = getUnexploredNeighbors(currentLoc, exploredAreaMap);
			for (Coordinate neighbor : neighbors) {
				if (isLandMass(map, neighbor))
					exploreLandQ.add(neighbor);
				else
					pathQ.add(neighbor);
			}
		}
	}
	
	private static List<Coordinate> getUnexploredNeighbors(Coordinate coord, boolean[][] exploredAreaMap) {
		List<Coordinate> validNeighbours = new LinkedList<Coordinate>();
		
		for (int[] direction : NEIGHBOUR_DIRECTIONS) {
			int deltaX = direction[0];
			int deltaY = direction[1];
			
			Coordinate neighbour = new Coordinate(coord.getRow() + deltaY, coord.getColumn() + deltaX);
			if (isInBound(neighbour, exploredAreaMap) && !isExplored(exploredAreaMap, neighbour))
				validNeighbours.add(neighbour);
		}
		return validNeighbours;
	}
	
	private static boolean isInBound(Coordinate coord, boolean[][] exploredAreaMap) {
		if (coord.getRow() >= exploredAreaMap.length || coord.getRow() < 0)
			return false;
		if (coord.getColumn() >= exploredAreaMap[coord.getRow()].length || coord.getColumn() < 0)
			return false;
		return true;
	}
	
	private static boolean isLandMass(int[][] map, Coordinate coord) {
		return map[coord.getRow()][coord.getColumn()] == 1;
	}
	
	public static boolean isExplored(boolean[][] matrix, Coordinate coord) {
		return matrix[coord.getRow()][coord.getColumn()];
	}
	
	final static int[][] NEIGHBOUR_DIRECTIONS = {
			{0, 1}, {0, -1}, {1, 0}, {-1, 0}
	};
}
