package matrix.maps;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class FindBiggestIslandSize {

	public int solution (int[][] map) {
		int biggestIslandSize = 0;
		Queue<Coordinate> travelQ = new LinkedList<Coordinate>();
		
		MatrixMap matrixMap = new MatrixMap(map);
		
		Coordinate startingLoc = new Coordinate(0,0);
		travelQ.add(startingLoc);
		
		while (!travelQ.isEmpty()) {
			Coordinate coordinate = travelQ.poll();
			if (matrixMap.isExplored(coordinate))
				continue;
			
			if (matrixMap.isLand(coordinate)) {
				int landSize = mapEncounteredLand(matrixMap, coordinate, travelQ);
				biggestIslandSize = biggestIslandSize > landSize ? biggestIslandSize : landSize;
				continue;
			}
			
			matrixMap.setExplored(coordinate);
			travelQ.addAll(matrixMap.getNeighbors(coordinate));
		}
		
		return biggestIslandSize;
	}

	private int mapEncounteredLand(MatrixMap map, Coordinate coordinate, Queue<Coordinate> restOfMapQ) {
		int landSize = 0;
		
		Queue<Coordinate> travelQ = new LinkedList<Coordinate>();
		travelQ.add(coordinate);
		
		while(!travelQ.isEmpty()) {
			coordinate = travelQ.poll();
			if (map.isExplored(coordinate))
				continue;
			
			if (map.isLand(coordinate)) {
				landSize++;
			}
			map.setExplored(coordinate);
			
			for (Coordinate neighbor : map.getNeighbors(coordinate)) {
				Queue<Coordinate> queue = map.isLand(neighbor) ? travelQ : restOfMapQ;
				queue.add(neighbor);
			}
		}
		
		return landSize;
	}

	class MatrixMap {
		int[][] mapMatrix;
		boolean[][] exploredArea;
		
		public MatrixMap (int[][] matrix) {
			this.mapMatrix = matrix;
			exploredArea = new boolean[matrix.length][];
			for (int i = 0; i < matrix.length; i++) {
				exploredArea[i] = new boolean[matrix[i].length];
			}
		}
		
		public boolean isExplored(Coordinate coordinate) {
			return exploredArea[coordinate.row][coordinate.column];
		}
		
		public void setExplored(Coordinate coordinate) {
			exploredArea[coordinate.row][coordinate.column] = true;
		}
		
		public boolean isLand(Coordinate coordinate) {
			return mapMatrix[coordinate.row][coordinate.column] == 1;
		}
		
		public boolean isInbound(Coordinate coordinate) {
			int row = coordinate.row;
			if (row < 0 || row >= mapMatrix.length)
				return false;
			int column = coordinate.column;
			if (column < 0 || column >= mapMatrix[row].length)
				return false;
			return true;
		}

		public List<Coordinate> getNeighbors(Coordinate coordinate) {
			List<Coordinate> validNeighbors = new LinkedList<Coordinate>();
			for (int[] delta : NEIGHBOURS_DELTA) {
				int deltaX = delta[0];
				int deltaY = delta[1];
				
				Coordinate potentialNeighbor = new Coordinate(coordinate.row + deltaY, coordinate.column + deltaX);
				if (isInbound(potentialNeighbor) && !isExplored(potentialNeighbor))
					validNeighbors.add(potentialNeighbor);
			}
			return validNeighbors;
		}
		
		final int[][] NEIGHBOURS_DELTA = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
	}
	
	class Coordinate {
		public int row;
		public int column;
		
		public Coordinate(int row, int column) {
			this.row = row;
			this.column = column;
		}
	}
}
