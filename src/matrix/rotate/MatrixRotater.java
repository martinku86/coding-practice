package matrix.rotate;

import java.util.ArrayList;

import concepts.Coordinate;

public class MatrixRotater {

	int[][] matrix;
	
	public MatrixRotater(int[][] matrix) {
		this.matrix = matrix;
	}
	
	public int[][] rotateClockWise() {
		// run rotate permeter until center
		int centerLevel = matrix.length / 2;
		
		for(int k = 0; k < centerLevel; k++) {
			rotatePerimeter(matrix, k);
		}
		
		return matrix;
	}
	
	private int[][] rotatePerimeter(int[][] squarePicture, int perimeterLevel) {
		int length = squarePicture.length - 1;

		//iterate from one corner to next
		for (int i = perimeterLevel; i < length - perimeterLevel; i++) {
			ArrayList<Coordinate> perimeterIterators = perimeterCounter(length, perimeterLevel, i);
			int prevBuf = perimeterIterators.get(3).getValueFromMatrix(squarePicture);		

			for (Coordinate coord : perimeterIterators) {
				int currBuf = coord.getValueFromMatrix(squarePicture);
				coord.setValueInMatrix(squarePicture, prevBuf);
				prevBuf = currBuf;
			}
		}
		return squarePicture;
	}
	
	private ArrayList<Coordinate> perimeterCounter(int length, int perimeterLevel, int iteration) {
		//returns array of 4 Coordinates
		ArrayList<Coordinate> resultList = new ArrayList<Coordinate>();
		
		//top left
		resultList.add( new Coordinate(perimeterLevel, perimeterLevel + iteration) );
		//top right
		resultList.add( new Coordinate(perimeterLevel + iteration, length - perimeterLevel) );
		//bottom right
		resultList.add( new Coordinate(length - perimeterLevel, length - perimeterLevel - iteration) );
		//bottom left
		resultList.add( new Coordinate(length - perimeterLevel - iteration, perimeterLevel) );
		
		return resultList;
	}
}
