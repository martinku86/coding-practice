package matrix.search;

public class MatrixSpiralIteration {
    public int[][] generateMatrix(int n) {
        if (n <= 0)
            return new int[0][0];

        SpiralNavigator nav = new BooleanSpiralNavigator(n);
        int count = 1;
        while (nav.setValueAndNavigateNext(count) > 0) {
            count++;
        }
        return nav.getMatrix();
    }

    interface SpiralNavigator {
        public int setValueAndNavigateNext(int count);
        
        public int[][] getMatrix();
    }

    class CornerSpiralNavigator implements SpiralNavigator {
        int[][] matrix;
        Coordinate[] corners;
        int cornerIndex;
        Direction currentDir;
        Coordinate currentCoord;
        int unexploredCount;
        
        public CornerSpiralNavigator(int n) {
            //set up cases for n = 0
            
            if (n > 0)
                matrix = new int[n][n];
            
            corners = new Coordinate[4];
            for (int i = 0; i < corners.length; i++) {
                switch(i) {
                    case 0:
                        corners[i] = new Coordinate(0, 0);
                        break;

                    case 1:
                        corners[i] = new Coordinate(0, matrix[0].length - 1);
                        break;

                    case 2:
                        corners[i] = new Coordinate(matrix.length - 1, matrix[matrix.length-1].length - 1);
                        break;

                    case 3:
                        corners[i] = new Coordinate(matrix.length - 1, 0);
                        break;

                    default:
                        break;
                }
            }
            
            cornerIndex = 0;
            unexploredCount = n * n;
            currentDir = getDirection(cornerIndex);
            cornerDestination = calculateCornerDestination(cornerIndex);
            currentCoord = new Coordinate(0,0);
        }

        Coordinate cornerDestination;
        public int setValueAndNavigateNext(int n) {
            if (unexploredCount == 0)
                return 0;
            
            setValue(currentCoord, n);

            if (currentCoord.equals(cornerDestination)) {
                // update corner
                cornerIndex = updateCorner(cornerIndex);
                currentCoord = corners[cornerIndex];
                currentDir = getDirection(cornerIndex);
                cornerDestination = calculateCornerDestination(cornerIndex);
            }
            else {
                currentCoord = currentCoord.goDirection(currentDir);
            }

            // update unexplored
            unexploredCount--;

            return unexploredCount;
        }

        private Coordinate calculateCornerDestination(int cornerIndex) {
            Coordinate destination = null;
            Coordinate corner = corners[cornerIndex];
            int n = matrix.length - 1;
            switch (cornerIndex) {
                case 0:
                    destination = new Coordinate(corner.y, n - corner.x - 1);
                    break;
                case 1:
                    destination = new Coordinate(n - corner.y - 1, corner.x);
                    break;
                case 2:
                    destination = new Coordinate(corner.y, n - corner.x + 1);
                    break;
                case 3:
                    destination = new Coordinate(n - corner.y + 1, corner.x);
                    break;
            }
            return destination;
        }

        private int updateCorner(int cornerIndex) {
            switch (cornerIndex) {
                case 0:
                    corners[0] = new Coordinate(corners[0].y + 1, corners[0].x + 1);
                    break;
                case 1:
                    corners[1] = new Coordinate(corners[1].y + 1, corners[1].x - 1);
                    break;
                case 2:
                    corners[2] = new Coordinate(corners[2].y - 1, corners[2].x - 1);
                    break;
                case 3:
                    corners[3] = new Coordinate(corners[3].y - 1, corners[3].x + 1);
                    break;

            }
            
            return cornerIndex + 1 > 3 ? 0 : cornerIndex + 1;
        }

        public void setValue(Coordinate coord, int val) {
            matrix[coord.y][coord.x] = val;
        }
        
        public Direction getDirection(int cornerIndex) {
            Direction dir = null;
            if (cornerIndex == 0) {
                dir = new Direction(0, 1);
            }
            if (cornerIndex == 1) {
                dir = new Direction(1, 0);
            }
            if (cornerIndex == 2) {
                dir = new Direction(0, -1);
            }
            if (cornerIndex == 3) {
                dir = new Direction(-1, 0);
            }

            return dir;
        }

        @Override
        public int[][] getMatrix() {
            return matrix;
        }
    }
    
    class BooleanSpiralNavigator implements SpiralNavigator {

        int[][] matrix;
        boolean[][] areaTracker;
        Coordinate currentCoord;
        int unexploredCount;

        int currentDirection;
        Direction[] directions = {
            new Direction(0, 1),
            new Direction(1, 0),
            new Direction(0, -1),
            new Direction(-1, 0)
        };

        public BooleanSpiralNavigator(int n) {
            if (n <= 0)
                return;

            matrix = new int[n][n];
            areaTracker = new boolean[n][n];
            currentDirection = 0;
            currentCoord = new Coordinate(0, 0);
            unexploredCount = n * n;
        }

        @Override
        public int setValueAndNavigateNext(int count) {
            if (unexploredCount < 1)
                return 0;
            
            setValue(currentCoord, count);
            markExplored(currentCoord);

            Coordinate nextCoord = currentCoord.goDirection(directions[currentDirection]);
            if (isOutOfBounds(nextCoord) || isExplored(nextCoord)) {
                changeDirection();
                nextCoord = currentCoord.goDirection(directions[currentDirection]);
            }
            
            currentCoord = nextCoord;
            unexploredCount--;
            return unexploredCount;
        }

        private void markExplored(Coordinate coord) {
            areaTracker[coord.y][coord.x] = true;
        }

        private boolean isOutOfBounds(Coordinate coord) {
            return (coord.y >= matrix.length || coord.y < 0) || (coord.x >= matrix.length || coord.x < 0);
        }

        private boolean isExplored(Coordinate coord) {
            return areaTracker[coord.y][coord.x];
        }

		public void setValue(Coordinate coord, int val) {
            matrix[coord.y][coord.x] = val;
        }

        private void changeDirection() {
            currentDirection = currentDirection >= 3 ? 0 : currentDirection + 1; 
        }

        @Override
        public int[][] getMatrix() {
            return matrix;
        }
    }

    class Coordinate {
        int x;
        int y;
        
        public Coordinate(int y, int x) {
            this.y = y;
            this.x = x;
        }
        
        public Coordinate goDirection(Direction dir) {
            return new Coordinate(this.y + dir.deltaY, this.x + dir.deltaX);
        }
        
        public boolean equals(Coordinate other) {
            return this.x == other.x && this.y == other.y;
        }
    }
    
    class Direction {
        int deltaX;
        int deltaY;
        
        public Direction(int deltaY, int deltaX) {
            this.deltaX = deltaX;
            this.deltaY = deltaY;
        }
    }
}
