package matrix.search;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Place N Queen on N x N sized chessboard where no queen can take each other
 * Original question - https://leetcode.com/problems/n-queens/
 * @author Martin
 *
 */
public class SolveNQueens {
    public List<List<String>> solveNQueens(int n) {
        Board board = new Board(n);
        
        return solveNQueensRecursive(n, board, new LinkedList<List<String>>(), 0);
    }
    
    private List<List<String>> solveNQueensRecursive(int n, Board board, List<List<String>> results, int row) {

        for (int i = 0; i < n; i++) {
            if (!board.getPos(row, i).isBlocked()) {
                // place queen
                board.placeQueen(row,  i);
                if (row == n-1) {
                    results.add(board.getResultString());
                }
                else {
                    solveNQueensRecursive(n, board, results, row+1);
                }
                board.unplaceQueen(row, i);
            }
        }
        return results;
    }
    
    public BlockMarker[] getBlockMarkerFromBoard(Board board, BoardPosition pos) {
        BlockMarker[] result = new BlockMarker[4];

        for (int i = 0; i < 4; i++) {
            BlockMarker blocker = null;
            switch (i) {
                case 0:
                    if (pos.getRow() == 0) {
                        blocker = new BlockMarker();
                    }
                    else {
                        blocker = board.positions[pos.getRow() - 1][pos.getCol()].vert;
                    }
                    break;
                case 1:
                    if (pos.getCol() == 0) {
                        blocker = new BlockMarker();
                    }
                    else {
                        blocker = board.positions[pos.getRow()][pos.getCol() - 1].hori;
                    }
                    break;
                case 2:
                    if (pos.getRow() == 0 || pos.getCol() == 0) {
                        blocker = new BlockMarker();
                    }
                    else {
                        blocker = board.positions[pos.getRow() - 1][pos.getCol() - 1].tlbr;
                    }
                    break;
                case 3:
                    if (pos.getRow() == 0 || pos.getCol() >= board.positions.length - 1) {
                        blocker = new BlockMarker();
                    }
                    else {
                        blocker = board.positions[pos.getRow() - 1][pos.getCol() + 1].trbl;
                    }
                    break;
            }
            result[i] = blocker;
        }
        
        return result;
    }
    
    class Board {
        BoardPosition[][] positions;
        int n;

        public Board(int n) {
            this.n = n;
            positions = new BoardPosition[n][];
            
            for (int i = 0; i < n; i++) {
                positions[i] = new BoardPosition[n];
                for (int j = 0; j < n; j++) {
                    positions[i][j] = new BoardPosition(i, j);
                    
                    BlockMarker[] markers = getBlockMarkerFromBoard(this, positions[i][j]);
                    
                    positions[i][j].registerBlockMarkers(markers[0], markers[1], markers[2], markers[3]);
                }
            }
        }
        
        public BoardPosition getPos(int row, int column) {
            return positions[row][column];
        }
        
        public void placeQueen(int row, int column) {
            positions[row][column].placeQueen();
        }
        
        public void unplaceQueen(int row, int column) {
            positions[row][column].unplaceQueen();
        }
        
        public List<String> getResultString() {
            List<String> result = new LinkedList<String>();
            
            StringBuilder builder = null;
            for (int i = 0; i < n; i++) {
                builder = new StringBuilder();
                for (int j = 0; j < n; j++) {
                    if (this.getPos(i, j).hasQueen())
                        builder.append('Q');
                    else 
                        builder.append('.');
                }
                
                result.add(builder.toString());
            }
            return result;
        }
    }
    
    class BoardPosition {
        int[] boardPos;
        int hashCode;
        boolean hasQueen;
        
        BlockMarker vert;
        BlockMarker hori;
        BlockMarker tlbr;
        BlockMarker trbl;
        
        public BoardPosition(int row, int column) {
            boardPos = new int[]{row, column};
            hashCode = Arrays.hashCode(boardPos);
            hasQueen = false;
        }
        
        @Override
        public int hashCode() {
            return hashCode;
        }
        
        public int getRow() {
            return boardPos[0];
        }
        
        public int getCol() {
            return boardPos[1];
        }
        
        public void registerBlockMarkers(BlockMarker vert, BlockMarker hori, BlockMarker tlbr, BlockMarker trbl) {
            this.vert = vert;
            this.hori = hori;
            this.tlbr = tlbr;
            this.trbl = trbl;
        }
        
        public boolean isBlocked() {
            return vert.isBlocked() || hori.isBlocked() || tlbr.isBlocked() || trbl.isBlocked();
        }
        
        public boolean hasQueen() {
            return hasQueen;
        }
        
        public void placeQueen() {
            vert.setBlocked(true);
            hori.setBlocked(true);
            tlbr.setBlocked(true);
            trbl.setBlocked(true);
            hasQueen = true;
        }
        
        public void unplaceQueen() {
            vert.setBlocked(false);
            hori.setBlocked(false);
            tlbr.setBlocked(false);
            trbl.setBlocked(false);
            hasQueen = false;
        }
    }
    
    public class BlockMarker {
        Set<BoardPosition> set;
        boolean blocked;
        
        public BlockMarker() {
            set = new HashSet<BoardPosition>();
            blocked = false;
        }
        
        public void registerBoardPosition(BoardPosition pos) {
            set.add(pos);
        }
        
        public void setBlocked(boolean blockedOrNot) {
            blocked = blockedOrNot;
        }
        
        public boolean isBlocked() {
            return blocked;
        }
    }
}