package matrix.search;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MatrixGridSearch {

    static class PatternChecker {
        String[] grid;
        String[] pattern;
        int gRow;
        int gCol;
        int pRow;
        int pCol;
        
        public PatternChecker(String[] grid, String[] pattern, int gRow, int gCol, int pRow, int pCol) {
            this.grid = grid;
            this.pattern = pattern;
            this.gRow = gRow;
            this.gCol = gCol;
            this.pRow = pRow;
            this.pCol = pCol;
        }
        
        public boolean checkPattern() {
            if (pattern.length == 0) {
                return false;
            }
            if (pCol == 0) {
                return (gCol == 0) && (gRow >= pRow);
            }
            
            char p = pattern[0].charAt(0);
            
            HashMap<Character, List<PatternCheckTracker>>[] currentLineTrackers = null;
            HashMap<Character, List<PatternCheckTracker>>[] nextLineTrackers = null;
            
            for (int i = 0; i < gRow; i++) {
                if (currentLineTrackers == null) {
                    nextLineTrackers = new HashMap[gCol];
                }
                currentLineTrackers = nextLineTrackers;
                nextLineTrackers = new HashMap[gCol];
                
                for (int j = 0; j < gCol; j++) {
                    if (i <= gRow - pRow &&
                        j <= gCol - pCol && 
                        grid[i].charAt(j) == p)  {
                        addTrackerToMap(currentLineTrackers, j, new PatternCheckTracker(pattern), p);
                    }
                    
                    if (currentLineTrackers[j] == null || !currentLineTrackers[j].containsKey(grid[i].charAt(j)))
                        continue;
                    
                    for (PatternCheckTracker tracker : currentLineTrackers[j].get(grid[i].charAt(j))) {
                        HashMap<Character, List<PatternCheckTracker>>[] trackerMapsArray = null;
                        int trackersMapIndex = 0;
                        if (tracker.atLineEnd()) {
                            trackerMapsArray = nextLineTrackers;
                            trackersMapIndex = j + 1 - pCol;
                        }
                        else {
                            trackerMapsArray = currentLineTrackers;
                            trackersMapIndex = j + 1;
                        }
                        tracker.advanceTracker();
                        if (tracker.isComplete())
                            return true;
                        addTrackerToMap(trackerMapsArray, trackersMapIndex, tracker, tracker.getCurrent());
                    }
                }
            }
            return false;
        }
        
        private void addTrackerToMap(HashMap<Character, List<PatternCheckTracker>>[] trackerMapsArray,
                                     int trackersMapIndex,
                                     PatternCheckTracker tracker,
                                     char c) {
            HashMap<Character, List<PatternCheckTracker>> trackerMap = null;
            if (trackerMapsArray[trackersMapIndex] == null) {
                trackerMapsArray[trackersMapIndex] = new HashMap<Character, List<PatternCheckTracker>>();
            }
            trackerMap = trackerMapsArray[trackersMapIndex];
            if (!trackerMap.containsKey(c))
                trackerMap.put(c, new LinkedList<PatternCheckTracker>());
            trackerMap.get(c).add(tracker);
        }
    }
    
    static class PatternCheckTracker {
        int line;
        int index;
        String[] pattern;
        
        public PatternCheckTracker(String[] pattern) {
            this.pattern = pattern;
            line = 0;
            index = 0;
        }

        public char getCurrent() {
            return pattern[line].charAt(index);
        }

        public void advanceTracker() {
            index++;
            if (index >= pattern[line].length()) {
                line++;
                index = 0;
            }
        }

        public boolean atLineEnd() {
            return index == pattern[line].length() - 1;
        }
        
        public boolean isComplete() {
            return line >= pattern.length;
        }
    }
    
}
