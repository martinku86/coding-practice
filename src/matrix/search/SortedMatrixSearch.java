package matrix.search;

/**
 * Problem from leetcode - https://leetcode.com/problems/search-a-2d-matrix-ii/
 * 
 * Current Idea - linear search and reduce search window by the sorted property
 * Improvment - window reduction can be done via binary search
 * 
 * @author Martin
 *
 */
public class SortedMatrixSearch {

	public static boolean searchMatrix(int[][] matrix, int target) {
        int rowUpper = matrix.length-1;
        int rowLower = 0;
        int colUpper = (matrix[rowUpper].length) - 1;
        int colLower = 0;
        
        int holder = 0;
        while (rowUpper >= rowLower && colUpper >= colLower) {
            // search upper row then col
            holder = matrix[rowUpper][colLower];
            if (holder == target) return true;
            if (holder > target)
                rowUpper--;
            
            holder = matrix[rowLower][colUpper];
            if (holder == target) return true;
            if (holder > target)
                colUpper--;
            
            if (rowUpper < rowLower || colUpper < colLower)
                return false;
            
            // search lower row then col
            holder = matrix[rowLower][colUpper];
            if (holder == target) return true;
            if (holder < target)
                rowLower++;
            
            holder = matrix[rowUpper][colLower];
            if (holder == target) return true;
            if (holder < target)
                colLower++;
        }
        
        return false;
    }
}
