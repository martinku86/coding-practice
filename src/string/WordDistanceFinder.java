package string;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class WordDistanceFinder {

	String[] sentence;
	Map<String, LinkedList<Integer>> indexByWord;
	
	public WordDistanceFinder(String[] sentence) {
		this.sentence = sentence;
		indexByWord = new HashMap<String, LinkedList<Integer>>();
		for (int i = 0; i < sentence.length; i++) {
			String word = sentence[i];
			if (!indexByWord.containsKey(word))
				indexByWord.put(word, new LinkedList<Integer>());
			indexByWord.get(word).add(i);
		}
	}
	
	public int findDistance(String word0, String word1) {
		int distance = -1;
		
		// check both words are in histogram
		if (!indexByWord.containsKey(word0) || !indexByWord.containsKey(word1)) {
			return distance;
		}
		
		// find distance
		LinkedList<Integer> leadingList = indexByWord.get(word0);
		LinkedList<Integer> trailingList = indexByWord.get(word1);

		while (!leadingList.isEmpty() && !trailingList.isEmpty()) {
			// establish leading and trailing lists
			if (leadingList.peek() > trailingList.peek()) {
				LinkedList<Integer> holder = leadingList;
				leadingList = trailingList;
				trailingList = holder;
			}
			
			// advance leading list until it is no longer leading (poll element) or empty
			int closetLeading = leadingList.poll();
			while (!leadingList.isEmpty() && (leadingList.peek() < trailingList.peek())) {
				closetLeading = leadingList.poll();
			}
			// the last polled element is closest to head of trailing list, compute distance
			distance = trailingList.peek() - closetLeading;
			if (distance == 1) {
				return distance;
			}
		}
		
		return distance;
	}
}
