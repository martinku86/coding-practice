package string;

public class SimpleRegexDotStar {
	public static boolean isMatch(String s, String p) {
        int sIndex = 0;
        int pIndex = 0;
        
        boolean isDot = false;
        boolean isStar = false;
        while (sIndex < s.length()) {
            if (pIndex >= p.length())
                return false;
            
            isDot = p.charAt(pIndex) == '.';
            isStar = (pIndex+1 < p.length()) && (p.charAt(pIndex+1) == '*');
            
            boolean currentCharMatch = (s.charAt(sIndex) == p.charAt(pIndex)) || isDot;
            if (isStar) {
                while (sIndex < s.length() && currentCharMatch) {
                    currentCharMatch = (s.charAt(sIndex) == p.charAt(pIndex)) || isDot;
                    if (currentCharMatch)
                        sIndex++;
                }
                int newIndex = pIndex + 2;
                while (newIndex < p.length() && p.charAt(newIndex) == p.charAt(pIndex)) {
                    newIndex++;
                }
                pIndex = newIndex;
            }
            else {
                if (!currentCharMatch)
                    return false;
                sIndex++;
                pIndex++;
            }
        }
        
        // you can have trailing regex
        // trailing regex might have *s which are valid
        // currrent does not account for this case - input "aaa", "ab*a*c*a"
        while (pIndex < p.length()) {
            isStar = (pIndex+1 < p.length()) && (p.charAt(pIndex+1) == '*');
            if (!isStar)
                return false;
            
        }
        
        return true;
    }
}
