package stack;

import java.util.Stack;

public class StackSorting {

	public static Stack<Integer> sortStack(Stack<Integer> stack) {
		Stack<Integer> sortedStack = new Stack<Integer>();
		sortedStack.push(stack.pop());
		
		while(!stack.empty()) {
			Integer currentInt = stack.pop();
			if (currentInt >= sortedStack.peek()) {
				sortedStack.push(currentInt);
			}
			else {
				while (!sortedStack.empty() && currentInt < sortedStack.peek()) {
					stack.push(sortedStack.pop());
				}
				sortedStack.push(currentInt);
			}
		}
		
		return sortedStack;
	}
}
