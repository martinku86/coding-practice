package stack;

import java.util.Stack;

public class TowerOfHanoiSolution {

	public static void solveTowerOfHanoidFirstToThird(Stack<Integer>[] towers) {
		Integer size = towers[0].size();
		moveFromTowerToTower(towers[0], towers[2], towers[1], size-1);
	}

	private static void moveFromTowerToTower(Stack<Integer> fromTower, Stack<Integer> toTower, Stack<Integer> bufferTower, Integer threshold) {
		if (threshold == 0) {
			moveRing(fromTower, toTower);
			return;
		}
		
		moveFromTowerToTower(fromTower, bufferTower, toTower, threshold-1);
		moveRing(fromTower, toTower);
		moveFromTowerToTower(bufferTower, fromTower, toTower, threshold-1);
	}

	private static Integer moveRing(Stack<Integer> fromTower, Stack<Integer> toTower) throws IllegalStateException {
		if (fromTower.empty())
			throw new IllegalStateException("No more rings from tower");
		
		if (!toTower.empty() && (fromTower.peek() > toTower.peek())) {
			throw new IllegalStateException("Ring from tower is bigger than destination tower");
		}
		
		Integer ring = toTower.pop();
		toTower.push(ring);
		return ring;
	}
}
