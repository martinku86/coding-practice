package permutation;

public class PrintAllPermutation {

	public void printAllPermutation(String string) {
		boolean[] availablePositions = new boolean[string.length()];
		for (int i = 0; i < availablePositions.length; i++)
			availablePositions[i] = true;
			
		printAllPositionPermutationRecursive(string, 0, new StringBuilder(), availablePositions);
	}

	public void printAllPositionPermutationRecursive(String string, int index, StringBuilder permuBuilder, boolean[] availablePositions) {
		if (index == string.length()) {
			System.out.println(permuBuilder.toString());
			return;
		}
		
		for (int i = 0; i < string.length() - index; i++) {
			int nextAvailable = getIthAvailablePosition(availablePositions, i);
			char c = string.charAt(nextAvailable);
			permuBuilder.append(c);
			availablePositions[nextAvailable] = false;
			
			printAllPositionPermutationRecursive(string, index+1, permuBuilder, availablePositions);
			
			//restore the properties
			permuBuilder.deleteCharAt(index);
			availablePositions[nextAvailable] = true;
		}
	}

	private int getIthAvailablePosition(boolean[] availablePositions, int i) {
		for (int j = 0; j < availablePositions.length; j++) {
			if (availablePositions[j])
				i--;
			if (i < 0)
				return j;
		}
		
		return -1;
	}
}
