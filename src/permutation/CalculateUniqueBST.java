package permutation;

public class CalculateUniqueBST {

	public static int solution(int n) {
		int numTrees = 0;
        int[] choicesToPermutationMap = new int[n+1];
        choicesToPermutationMap[0] = 1;

        for (int i = 1; i <= n; i++) {
            numTrees += calculateTreesRecursive(i, 1, n, choicesToPermutationMap);
        }

        return numTrees;
	}
	

    private static int calculateTreesRecursive(int node, int leftBound, int rightBound, int[] choicesToPermutationMap) {
        int leftChoices = node - leftBound;

        if (leftChoices != 0 && choicesToPermutationMap[leftChoices] == 0) {
            int updateLeft = 0;
            for (int i = 0; i < leftChoices; i++) {
                updateLeft += calculateTreesRecursive(leftBound+i, leftBound, node-1, choicesToPermutationMap);
            }
            choicesToPermutationMap[leftChoices] = updateLeft;
        }
        leftChoices = choicesToPermutationMap[leftChoices];

        int rightChoices = rightBound - node;
        if (rightChoices != 0 && choicesToPermutationMap[rightChoices] == 0) {
            int updateRight = 0;
            for (int i = 0; i < rightChoices; i++) {
                updateRight += calculateTreesRecursive(node+1+i, node+1, rightBound, choicesToPermutationMap);
            }
            choicesToPermutationMap[rightChoices] = updateRight;
        }
        rightChoices = choicesToPermutationMap[rightChoices];
        
        return leftChoices * rightChoices;
    }
}
