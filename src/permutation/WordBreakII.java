package permutation;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WordBreakII {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        BitSet wordSizes = new BitSet();
        
        for (String word : wordDict) {
            wordSizes.set(word.length());
        }
        
        LinkedList<LinkedList<Integer>> comboes = wordBreakRecursive(s, wordDict, new WordBreakDynamicCache(s), 0, wordSizes);
        
        List<String> result = new LinkedList<String>();
        if (comboes == null)
            return result;
        
        for (LinkedList<Integer> combo : comboes) {
            StringBuilder builder = new StringBuilder();
            Iterator<Integer> iter = combo.iterator();
            int start = 0;
            int next = 0;
            while (iter.hasNext()) {
                start = next;
                next = iter.next();
                
                builder.append(s.substring(start, next));
                if (iter.hasNext())
                    builder.append(" ");
            }
            result.add(builder.toString());
        }
        return result;
    }
    
    private LinkedList<LinkedList<Integer>> wordBreakRecursive(String s, Set<String> wordDict, WordBreakDynamicCache cache, int index, BitSet wordSizes) {
        if (index >= s.length()) {
            return null;
        }
        if (cache.explored(index)) {
            if (cache.hasCombo(index)) {
                return cache.getCombo(index);
            }
            else {
                return null;
            }
        }
        
        LinkedList<LinkedList<Integer>> thisIndexResult = new LinkedList<LinkedList<Integer>>();
        
        //explore the rest
        for (int i = wordSizes.nextSetBit(0);
        		i >= 0 && i + index <= s.length();
        		i = wordSizes.nextSetBit(i+1)) {
        	int nextIndex = i + index;
            String candidateWord = s.substring(index, nextIndex);
            
            if (wordDict.contains(candidateWord)) {
                LinkedList<LinkedList<Integer>> restOfStringWordsCombo = null;
                
                boolean hasCombo = false;
                if (nextIndex != s.length()) {
                    restOfStringWordsCombo = wordBreakRecursive(s, wordDict, cache, nextIndex, wordSizes);
                    hasCombo = restOfStringWordsCombo != null ? true : false;
                }
                else {
                    hasCombo = true;
                }
                
                LinkedList<LinkedList<Integer>> currentIndexWords = null;
                if (hasCombo) {
                    currentIndexWords = new LinkedList<LinkedList<Integer>>();
                    
                    if (restOfStringWordsCombo != null) {
                        for (LinkedList<Integer> words : restOfStringWordsCombo) {
                            LinkedList<Integer> wordsCopy = new LinkedList<Integer>();
                            for (Integer word : words) {
                                wordsCopy.add(word);
                            }
                            currentIndexWords.add(wordsCopy);
                        }
                    }
                    else {
                        currentIndexWords.add(new LinkedList<Integer>());
                    }
                    
                    for (LinkedList<Integer> wordsCopy : currentIndexWords) {
                        wordsCopy.addFirst(nextIndex);
                        thisIndexResult.add(wordsCopy);
                    }
                }
                
            }
            else {
                continue;
            }
        }
        
        if (thisIndexResult.isEmpty()) {
            return null;
        }
        
        cache.cacheResult(index, true, thisIndexResult);
        return thisIndexResult;
    }
    
    class WordBreakDynamicCache {
        boolean[] exploredFlags;
        boolean[] hasCombo;
        ArrayList<LinkedList<LinkedList<Integer>>> comboCache;
        
        public WordBreakDynamicCache(String s) {
            exploredFlags = new boolean[s.length()];
            hasCombo = new boolean[s.length()];
            comboCache = new ArrayList<LinkedList<LinkedList<Integer>>>(s.length());
            for (int i = 0; i < s.length(); i++) {
                comboCache.add(null);
            }
        }
        
        public boolean explored(int i) {
            return exploredFlags[i];
        }
        
        public boolean hasCombo(int i) {
            return hasCombo[i];
        }
        
        public LinkedList<LinkedList<Integer>> getCombo(int i) {
            return comboCache.get(i);
        }
        
        public void cacheResult(int i, boolean valid, LinkedList<LinkedList<Integer>> list) {
            exploredFlags[i] = true;
            if (!valid) {
                hasCombo[i] = false;
                return;
            }
            hasCombo[i] = true;
            comboCache.set(i, list);
        }
    }
}
