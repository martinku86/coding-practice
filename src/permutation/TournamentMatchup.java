package permutation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TournamentMatchup {

	int[] teams;
	public TournamentMatchup(int[] teams) {
		this.teams = teams;
	}
	
	public LinkedList<List<Matchup>> generateMatchUps() {
		LinkedList<List<Matchup>> matchups = new LinkedList<List<Matchup>>();
		boolean oddTeams = teams.length % 2 == 0;
		int tableCount = oddTeams ?(teams.length / 2) + 1 : teams.length / 2;

		boolean homeStand = true;
		Map<Integer, Integer> tableMap = new HashMap<Integer, Integer>();
		for (int i = 0; i < teams.length - 1; i++) {
			List<Matchup> dailyMatches = new LinkedList<Matchup>();
			for (int j = 0; j < teams.length; j++) {
				int tableSpot = calculateRotation(j, tableCount, i);
				if (!tableMap.containsKey(tableSpot)) {
					tableMap.put(tableSpot, teams[j]);
				}
				else {
					int homeTeam = homeStand ? tableMap.get(tableSpot) : teams[j];
					int awayTeam = homeStand ? teams[j] : tableMap.get(tableSpot);
					dailyMatches.add(new Matchup(homeTeam, awayTeam));
				}
			}
			matchups.add(dailyMatches);
			homeStand = homeStand ? false : true;
			tableMap = new HashMap<Integer, Integer>();
		}

		return matchups;
	}

	private int calculateRotation(int team, int tableCount, int rotation) {
		if (team == 0)
			return 0;
		
		int tableSpot = team + rotation;
		if (tableSpot >= tableCount) {
			tableSpot = tableCount - (tableSpot - tableCount);
		}
		if (tableSpot < 0) {
			tableSpot = Math.abs(tableSpot);
		}
		return tableSpot;
	}

	public static class Matchup {
		int home;
		int visit;
		
		public Matchup(int home, int visit) {
			this.home = home;
			this.visit = visit;
		}
		
		@Override
		public int hashCode() {
			return toString().hashCode();
		}
		
		public String toString() {
			int greater = home;
			int lesser = visit;
			if (home < visit) {
				greater = visit;
				lesser = home;
			}

			return String.format("%d%d", greater, lesser);
		}
		
		public String printMatchUpLine() {
			return String.format("%d @ %d", visit, home);
		}
	}
}
