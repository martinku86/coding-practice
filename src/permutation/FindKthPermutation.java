package permutation;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/permutation-sequence/
 * 
 * The set [1,2,3,�,n] contains a total of n! unique permutations.
 * By listing and labeling all of the permutations in order,
 * We get the following sequence (ie, for n = 3):
 * "123"
 * "132"
 * "213"
 * "231"
 * "312"
 * "321"
 * Given n and k, return the kth permutation sequence.
 * Note: Given n will be between 1 and 9 inclusive.
 * 
 * @author Martin
 *
 */
public class FindKthPermutation {

    Map<Integer, Integer> factorialMap;
    public String getPermutation(int n, int k) {
        int[] elementPosArray = new int[n];
        for (int i = 0; i < n; i++)
            elementPosArray[i] = i+1;
        
        elementPosArray = findPermutationBySwap(n, k-1, elementPosArray);
        
        StringBuilder builder = new StringBuilder();
        for(int element : elementPosArray) {
            builder.append(element);
        }
        return builder.toString();
    }
    
    private int[] findPermutationBySwap(int n, int k, int[] elementPosArray) {
        int swapFactor = 0;
        int swapIndex = 0;
        int remainingSwap = k;
        
        for (int i = 0; i < elementPosArray.length; i++) {
            swapFactor = calculateFactorialDynamic(n - i) / (n - i);
            swapIndex = (remainingSwap / swapFactor) + i;
            
            elementPosArray = shiftPosition(swapIndex, i, elementPosArray);
            
            remainingSwap = remainingSwap % swapFactor;
            if (remainingSwap == 0)
                break;
        }
        
        return elementPosArray;
    }
    
    private int calculateFactorialDynamic(int n) {
        if (factorialMap == null)        
            factorialMap = new HashMap<Integer, Integer>();

        if (factorialMap.containsKey(n))
            return factorialMap.get(n);
        
        int returnValue = 0;
        if (n == 1 || n == 0) {
            returnValue = 1;
        }
        else {
            returnValue = n * calculateFactorialDynamic(n-1);
        }
        
        factorialMap.put(n, returnValue);
        return returnValue;
    }
    
    private int[] shiftPosition(int fromPosition, int toPosition, int[] elementPosArray) {
        int buffer = elementPosArray[fromPosition];
        
        for (int i = fromPosition; i > toPosition; i--) {
            elementPosArray[i] = elementPosArray[i-1];
        }
        elementPosArray[toPosition] = buffer;

        return elementPosArray;
    }
}
