package concepts;

public class BinaryTreeNode<T> {

	private T value;
	private BinaryTreeNode leftChild;
	private BinaryTreeNode rightChild;

	public BinaryTreeNode(T value) {
		this.setValue(value);
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public BinaryTreeNode getLeftChidld() {
		return leftChild;
	}

	public void setLeftChidld(BinaryTreeNode leftChidld) {
		this.leftChild = leftChidld;
	}

	public BinaryTreeNode getRightChidld() {
		return rightChild;
	}

	public void setRightChidld(BinaryTreeNode rightChidld) {
		this.rightChild = rightChidld;
	}

	
}
