package concepts;

public class Coordinate {
	int row;
	int column;
	
	public Coordinate(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getValueFromMatrix(int[][] matrix) {
		return matrix[row][column];
	}

	public void setValueInMatrix(int[][] matrix, int value) {
		matrix[row][column] = value;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (this.row == ((Coordinate)obj).row && this.column == ((Coordinate)obj).column);
	}
}
