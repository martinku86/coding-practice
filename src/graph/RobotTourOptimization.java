package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class RobotTourOptimization {

	int[][] adjMatrix;
	public RobotTourOptimization(int[][] adjMatrix) {
		this.adjMatrix = adjMatrix;
	}

	public LinkedList<Integer> getOrder() {
		LinkedList<Integer> resultOrderList = new LinkedList<Integer>();
		
		if (adjMatrix.length < 2)
			return resultOrderList;
		
		int[] verticeCost = new int[adjMatrix.length];
		
		int nextVertice = 0;
		for (int i = 0; i < adjMatrix.length - 1; i++) {
			for (int j = i+1; j < adjMatrix.length; j++) {
				verticeCost[i] += adjMatrix[i][j];
				verticeCost[j] += adjMatrix[j][i];
			}
		}
		
		Set<Integer> remainingVertice = new HashSet<Integer>();
		for (int i = 0; i < adjMatrix.length; i++) {
			remainingVertice.add(i);
		}
		
		for (int i = 0; i < verticeCost.length; i++) {
			nextVertice = verticeCost[nextVertice] > verticeCost[i] ? nextVertice : i;
		}
		
		while (!remainingVertice.isEmpty()) {
			resultOrderList.add(nextVertice);
			remainingVertice.remove(nextVertice);
			verticeCost[nextVertice] = -1;
			
			if (remainingVertice.isEmpty())
				break;
			
			float bestEffeciency = 0;
			int nextEffecientVertice = -1;
			for (int remainVertice : remainingVertice) {
				verticeCost[remainVertice] -= adjMatrix[nextVertice][remainVertice];
				float effeciency = (float) verticeCost[remainVertice] / adjMatrix[nextVertice][remainVertice];
				if (effeciency >= bestEffeciency) {
					nextEffecientVertice = remainVertice;
					bestEffeciency = effeciency;
				}
			}
			
			nextVertice = nextEffecientVertice;
		}
		
		return resultOrderList;
	}
}
