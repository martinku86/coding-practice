package graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;

public class LargestNumberFromCombination {

	/**
	 * Given a list of non negative integers, arrange them such that they form the largest number.
	 * For example, given [3, 30, 34, 5, 9], the largest formed number is 9534330.
	 * Note: The result may be very large, so you need to return a string instead of an integer.
	 */
	
	//TODO - redo the solution, use insert time analysis
	
	/**
	 * This solution categorizes certain attributes of each integer and uses it enhance comparison
	 * @param numbers
	 * @return
	 */
	public String generateLargestNumber(int[] numbers) {
		Map<Integer, TreeSet<NumberMarker>> map = new HashMap<Integer, TreeSet<NumberMarker>>();
		int highestSigFig = -1;
		for ( int number : numbers) {
			NumberMarker marker = new NumberMarker(number);

			if (!map.containsKey(marker.significantDigit))
				map.put(marker.getSignificantDigit(), new TreeSet<NumberMarker>());
			TreeSet<NumberMarker> treeSet = map.get(marker.getSignificantDigit());
			treeSet.add(marker);
			highestSigFig = highestSigFig  > marker.getSignificantDigit() ? highestSigFig : marker.getSignificantDigit();
		}
		
		StringBuilder builder = new StringBuilder();
		while (highestSigFig >= 0) {
			if (!map.containsKey(highestSigFig)) {
				highestSigFig--;
				continue;
			}
			
			TreeSet<NumberMarker> treeSet = map.get(highestSigFig);
			builder.append(treeSet.pollLast().stringRep);
			if (treeSet.isEmpty())
				map.remove(highestSigFig);
		}
		return builder.toString();
	}

	class NumberMarker implements Comparable<NumberMarker > {
		String stringRep;
		int number;
		int significantDigit;
		int lowerDigitMarker;

		public NumberMarker(int number) {
			this.number = number;
			stringRep = new Integer(number).toString();
			processNumber();
		}

		private void processNumber() {
			significantDigit = convertCharToInt(stringRep.charAt(0));

			for (int i = 0; i < stringRep.length(); i++) {
				Character otherDigitChar = stringRep.charAt(i);
				int otherDigit = convertCharToInt(otherDigitChar);
				if ( otherDigit == significantDigit ) {
					lowerDigitMarker = i;
				}
				else {
					lowerDigitMarker = i;
					break;
				}
			}
		}

		public int compareLowerDigitToSigFigValue() {
			return getLowerDigitValue() - significantDigit;
		}

		public int getLowerDigitValue() {
			return convertCharToInt(stringRep.charAt(lowerDigitMarker));
		}

		public int getSignificantDigit() {
			return significantDigit;
		}

		private boolean oneHigherOneLower(NumberMarker other) {
			return !((this.compareLowerDigitToSigFigValue() > 0 && other.compareLowerDigitToSigFigValue() > 0)
					|| (this.compareLowerDigitToSigFigValue() == 0 && other.compareLowerDigitToSigFigValue() == 0)
					|| (this.compareLowerDigitToSigFigValue() < 0 && other.compareLowerDigitToSigFigValue() < 0));
		}
		public int compareTo(NumberMarker other) {
			if (oneHigherOneLower(other)) {
				return this.compareLowerDigitToSigFigValue() - other.compareLowerDigitToSigFigValue();
			}
			

			int difference = this.getLowerDigitValue() - other.getLowerDigitValue();
			int positionDiff = other.lowerDigitMarker - this.lowerDigitMarker;

			if (positionDiff != 0) {
				int caseByLowerDigitCompare = compareLowerDigitToSigFigValue();
				if (caseByLowerDigitCompare > 0) {
					return positionDiff;
				}
				else if (caseByLowerDigitCompare == 0) {
					return -1 * positionDiff;
				}
				else if (caseByLowerDigitCompare < 0) {
					return -1 * positionDiff;
				}
			}
			else {
				if (difference != 0)
					return difference;
			}

			int i = this.stringRep.length() < other.stringRep.length() ? this.stringRep.length() : other.stringRep.length();

			for (int j = 0; j < i; j++) {
				int thisDigit = convertCharToInt(this.stringRep.charAt(j));
				int otherDigit = convertCharToInt(other.stringRep.charAt(j));
				difference = thisDigit - otherDigit;
				if (difference != 0) {
					return difference;
				}
			}

			// tieBreaker
			if (this.stringRep.length() == other.stringRep.length())
				return 0;

			boolean thisIsShorter = this.stringRep.length() < other.stringRep.length();
			String longerString = this.stringRep.length() > other.stringRep.length() ? this.stringRep : other.stringRep;

			for (int k = i; k < longerString.length(); k++) {
				int longerDigit = convertCharToInt(longerString.charAt(k));
				difference = longerDigit - significantDigit;
				if (difference != 0)
					break;
			}
			
			int diffModifier = thisIsShorter ? -1 : 1;
			if (difference == 0) {
				return diffModifier;
			}
			return diffModifier  * difference;
			
		}

		private int convertCharToInt(char c) {
			return Character.getNumericValue(c);
		}
	}

	
	
	/**
	 * Below solution is attempt using Trie
	 * @param nums
	 * @return
	 */
    public String largestNumberByTrie(int[] nums) {
    	SignificantDigitTrie trie = new SignificantDigitTrie();

         for( int num : nums) {
             trie.insert(num);
         }

         StringBuilder builder = new StringBuilder();
         while (!trie.isEmpty()) {
             LinkedList<Character> nextIntegerCandidate = trie.pollNextLexigraphicalInteger();
             for (Character c : nextIntegerCandidate)
                 builder.append(c);
         }
         return builder.toString();
    }

    class SignificantDigitTrie {
    	SignificantDigitTrieNode root;
    	public SignificantDigitTrie() {
    		root = new SignificantDigitTrieNode('0');
    	}

    	public void insert(Integer integer) {
    		String integerStringRep = integer.toString();
    		root.insert(integerStringRep);
    	}

    	public LinkedList<Character> pollNextLexigraphicalInteger() {
    		return root.pollWithHighestDigit(root.highestNextNodeDigit);
    	}
    	
    	public boolean isEmpty() {
    		return root.children.isEmpty();
    	}
    }

    class SignificantDigitTrieNode {
    	Character digit;
    	Integer pathCount;
    	Map<Character, SignificantDigitTrieNode> children;
    	Integer highestNextNodeDigit;

    	public SignificantDigitTrieNode(Character digit) {
    		this.digit = digit;
    		pathCount = 0;
    		children = new HashMap<Character, SignificantDigitTrieNode>();
    		highestNextNodeDigit = -1;
    	}

    	public SignificantDigitTrieNode getChildNode(Character c) {
    		if (!children.containsKey(c))
    			return null;
    		
    		return children.get(c);
    	}
    	
    	public void insert(String integerStringRep) {
    		char[] integerCharArray = integerStringRep.toCharArray();

    		SignificantDigitTrieNode childNode;
    		char c = integerCharArray[0];

    		if (!children.containsKey(c))
    			children.put(c, new SignificantDigitTrieNode(c));
    		childNode = children.get(c);

    		Integer childNodeValue = childNode.getDigitNumerical();
    		highestNextNodeDigit = highestNextNodeDigit >= childNodeValue ? highestNextNodeDigit : childNodeValue;
    			
    		if (integerCharArray.length <= 1) {
    			childNode.pathCount++;
    			return;
    		}
    		childNode.insert(integerStringRep.substring(1, integerStringRep.length()));
    	}

    	public LinkedList<Character> pollWithHighestDigit(Integer highestSignificantDigit) {
    		boolean hasSingleDigit = pathCount > 0;
    		boolean hasChildren = !children.isEmpty();
    		LinkedList<Character> highestChildrenList = null;
    		
    		// if has single digit, check if it has child >= to current digit
    		if (hasSingleDigit) {
    			if (hasChildren) {
    				boolean hasChildGreaterThanSigFig = highestNextNodeDigit > highestSignificantDigit;
    				if (hasChildGreaterThanSigFig) {
    					highestChildrenList = getNextHighestNode().pollWithHighestDigit(highestSignificantDigit);
    				}
    			}
    			pathCount--;
    			highestChildrenList = new LinkedList<Character>();
    			highestChildrenList.add(digit);
    			return highestChildrenList;
    		}
    		else {
    			highestChildrenList = getNextHighestNode().pollWithHighestDigit(highestSignificantDigit);
    		}
    		
    		// clean up nextHighest child node if required
    		if (getNextHighestNode().children.isEmpty() && getNextHighestNode().pathCount < 1 ) {
    			children.remove(highestNextNodeDigit.toString().charAt(0));
    			highestNextNodeDigit = -1;
    			for (Character c : children.keySet()) {
    					Integer i = Character.getNumericValue(c);
    					highestNextNodeDigit = highestNextNodeDigit > i ? highestNextNodeDigit : i;
    			}
    		}
    		
    		highestChildrenList.addFirst(digit);
    		return highestChildrenList;
    	}

    	private SignificantDigitTrieNode getNextHighestNode() {
    		Character c = highestNextNodeDigit.toString().charAt(0);
    		return children.get(c);
    	}

    	public Integer getDigitNumerical() {
    		return Character.getNumericValue(digit);
    	}
    }
}
