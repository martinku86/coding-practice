package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class LargestLexigraphicalStringSwap {

	public String findLargestSwappedString(String input, LinkedList<Indice> indices) {
		Map<Integer, Map<Integer, LinkedList<Integer>>> swapGraph = new HashMap<Integer, Map<Integer, LinkedList<Integer>>>();
		//build the 1st level graph
		for ( Indice indice : indices ) {
			
			if ( !swapGraph.containsKey( indice.front ) ) {
				swapGraph.put(indice.front, new HashMap<Integer, LinkedList<Integer>>());
			}
			if ( !swapGraph.containsKey( indice.back ) ) {
				swapGraph.put(indice.back, new HashMap<Integer, LinkedList<Integer>>());
			}

			Map<Integer, LinkedList<Integer>> pathMap0 = swapGraph.get(indice.front);
			if ( !pathMap0.containsKey(indice.back) ) {
				LinkedList<Integer> path0 = new LinkedList<Integer>();
				path0.add(indice.back);
				pathMap0.put(indice.back, path0);
			}
			Map<Integer, LinkedList<Integer>> pathMap1 = swapGraph.get(indice.back);
			if ( !pathMap1.containsKey(indice.front)) {
				LinkedList<Integer> path1 = new LinkedList<Integer>();
				path1.add(indice.front);
				pathMap1.put(indice.front, path1);
			}
		}

		// expand to secondary paths
		for (Integer topLevelIndex : swapGraph.keySet()) {
			BfsMapPath(swapGraph, topLevelIndex);
		}

		// start from first position then search the mapped paths for highest value swap
		char[] inputCharArray = input.toCharArray();
		for (int i = 0; i < input.length(); i++) {
			
			if (!swapGraph.containsKey(i+1))
				continue;
			
			Character indexChar = inputCharArray[i];
			Integer charValue = Character.getNumericValue(indexChar);
			Integer swapCandidate = i;
			
			// once found, perform rotation. remove the mapped path from graph.
			Set<Integer> swapPositions = swapGraph.get(i+1).keySet();
			
			for (Integer swapPosition : swapPositions) {
				// check if swapPosition has a higher value
				Character targetChar = inputCharArray[swapPosition-1];
				
				// update value is higher, track highest position
				if (Character.getNumericValue(targetChar) > charValue) {
					charValue = Character.getNumericValue(targetChar);
					swapCandidate = swapPosition -1;
				}
			}

			// swap with highest value position
			if (swapCandidate != i) {
				inputCharArray[i] = inputCharArray[swapCandidate];
				inputCharArray[swapCandidate] = indexChar;
			}
			// remove the paths going to index
			Set<Integer> targetsToIndex = swapGraph.get(i+1).keySet();
			for (Integer targetToIndex : targetsToIndex) {
				swapGraph.get(targetToIndex).remove(i+1);
			}
		}
		
		return new String(inputCharArray);
	}

	public void BfsMapPath(Map<Integer, Map<Integer, LinkedList<Integer>>> swapGraph, Integer index) {
		Set<Integer> circularCheckSet = new HashSet<Integer>();
		circularCheckSet.add(index);
		Queue<Integer> bfsQ = new LinkedList<Integer>(swapGraph.get(index).keySet());
		Map<Integer, LinkedList<Integer>> indexTargetMap = swapGraph.get(index);
		
		while(!bfsQ.isEmpty()) {
			Integer swapTarget = bfsQ.poll();

			if (circularCheckSet.contains(swapTarget))
				continue;
			else
				circularCheckSet.add(swapTarget);
			
			LinkedList<Integer> indexToTargetPath = (LinkedList<Integer>) indexTargetMap.get(swapTarget).clone();
			
			for(Integer nextTarget : swapGraph.get(swapTarget).keySet()) {
				if (circularCheckSet.contains(nextTarget))
					continue;
				
				indexToTargetPath.add(nextTarget);
				indexTargetMap.put(nextTarget, indexToTargetPath);
				// queue nextTarget
				bfsQ.offer(nextTarget);
			}
		}
	}

	public static class Indice {
		int front;
		int back;
		
		public Indice(int[] indiceArray) {
			this.front = indiceArray[0];
			this.back = indiceArray[1];
		}
		
		@Override
		public int hashCode() {
			return front + back;
		}
	}
}
