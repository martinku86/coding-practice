package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class TopologicalSearch {
	boolean[][] adjMatrix;

	public TopologicalSearch(boolean[][] adjMatrix) {
		this.adjMatrix = adjMatrix;
	}
	
	public LinkedList<Integer> search() {
		LinkedList<Integer> resultList = new LinkedList<Integer>();
		
		int[] dependencyFlags = getDependencyFlags();
		
		Queue<Integer> queue = new LinkedList<Integer>();
		
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < adjMatrix.length; i++) {
			set.add(i);
		}
		
		do {
			// search for steps with no incoming dependency
			while (!queue.isEmpty()) {
				int step = queue.poll();
				
				dependencyFlags = clearDependencyFor(dependencyFlags, step);
				resultList.add(step);
				set.remove(step);
			}
			
			for (int i = 0; i < adjMatrix.length; i++) {
				if (dependencyFlags[i] == 0 && set.contains(i)) {
					queue.add(i);
				}
			}
		} while (!queue.isEmpty());
		
		return resultList;
	}
	
	private int[] getDependencyFlags() {
		int[] dependencyFlags = new int[adjMatrix.length];
		for (int i = 0; i < adjMatrix.length; i++) {
			int dependencyFlag = 0;
			for (int j = 0; j < adjMatrix.length; j++) {
				if (adjMatrix[j][i]) {
					dependencyFlag = dependencyFlag | (1 << j);
				}
			}
			dependencyFlags[i] = dependencyFlag;
		}
		
		return dependencyFlags;
	}
	
	private int[] clearDependencyFor(int[] flags, int step) {
		for (int i = 0; i < flags.length; i++) {
			flags[i] = flags[i] & (~(1 << step));
		}
		return flags;
	}
}
