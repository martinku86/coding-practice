package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class WordLadder {
    public int ladderLength(String beginWord, String endWord, Set<String> wordList) {
        AdjacencyGraph adjGraph = new AdjacencyGraph(beginWord, endWord, wordList);
        
        return adjGraph.dijkstraSearch(beginWord, endWord);
    }
    
    class AdjacencyGraph {
        Map<String, List<String>> adjMap;
        
        public AdjacencyGraph(String beginWord, String endWord, Set<String> wordList) {
        	adjMap = new HashMap<String, List<String>>();
            updateAdjacency(beginWord);

            updateAdjacency(endWord);
            
            for (String word : wordList) {
                updateAdjacency(word);
            }
        }
        
        public int dijkstraSearch(String beginWord, String endWord) {
            int dijCost = 0;

            Set<String> visited = new HashSet<String>();
            Queue<String> explorationQ = new LinkedList<String>();
            explorationQ.add(beginWord);

            while (!explorationQ.isEmpty()) {
                Queue<String> nextQ = new LinkedList<String>();
                dijCost++;

                while (!explorationQ.isEmpty()) {
                    String currentWord = explorationQ.poll();
                    visited.add(currentWord);

                    List<String> adjacentList = adjMap.get(currentWord);
                    for (String neighbor : adjacentList) {
                        if (neighbor == endWord)
                            return dijCost;
                        
                        if (!visited.contains(neighbor)) {
                        	nextQ.add(neighbor);
                        	visited.add(neighbor);
                        }
                    }
                }
                
                explorationQ = nextQ;
            }
            
            return 0;
        }
        
        private void updateAdjacency(String word) {
            if (adjMap.containsKey(word))
                return;

            List<String> adjList = new LinkedList<String>();

            for (String otherWord : adjMap.keySet()) {
                int diffCount = 0;
                for (int i = 0; i < word.length(); i++) {
                    if (word.charAt(i) != otherWord.charAt(i)) {
                        diffCount++;
                        if (diffCount > 1)
                            break;
                    }
                }

                // is adjacent, update both this word and other word
                if (diffCount <= 1) {
                    adjList.add(otherWord);
                    adjMap.get(otherWord).add(word);
                }
            }

            adjMap.put(word, adjList);
        }
    }
}
