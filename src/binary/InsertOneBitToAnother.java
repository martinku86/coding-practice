package binary;

public class InsertOneBitToAnother {

	/**
	 * Question from Cracking the Coding Interview 6th Edition, Ch 5 Q5.1
	 * 
	 * You are given two 32 bit number N and M, two bit position i and j
	 * write method to insert M into N such that M starts at bit j ends at bit i.
	 * You can assume that bits j through i ahve enough space to fit all of M
	 * 
	 * e.g. Input N = 1000000000, M = 10011, i = 2, j = 6
	 * Ouput N = 10001001100 
	 * 
	 * @param m
	 * @param n
	 * @param i
	 * @param j
	 * @return
	 */
	public static int solution(String m, String n, int i, int j) {
		// clear m from i to j
		int clearMaskJ = ~0 << j;
		int clearMaskI = ~(~0 << i);
		int clearMask = clearMaskJ | clearMaskI;
		
		int mInt = Integer.parseInt(m, 2);
		mInt = mInt & clearMask;
		
		// shift n by i
		int nInt = Integer.parseInt(n, 2);
		nInt = nInt << i;
		
		// set shifted n into m
		mInt = mInt | nInt;
		
		// return result
		return mInt;
	}
}
