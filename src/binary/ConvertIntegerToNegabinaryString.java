package binary;

/*
 * CareerCup question - https://www.careercup.com/question?id=5085545090777088
 * Negabinary conversions - https://en.wikipedia.org/wiki/Negative_base#To_Negabinary
 * 
 * Current solution does not work
 */
public class ConvertIntegerToNegabinaryString {
	public static String solution(int i) {
		int shiftCount = 0;
		StringBuilder builder = new StringBuilder();

		boolean isPositive = (i >= 0);

		i = Math.abs(i);
		do {
			if ( (1 & i) == 1)
				builder.append(1);
			else
				builder.append(0);

			if ((!isPositive && isEven(shiftCount)) || (isPositive && !isEven(shiftCount)))
				i = i + 2;

			i = i >> 1;
			shiftCount++;
		} while (i != 0);

		return builder.reverse().toString();
	}

	public static boolean isEven(int shiftCount) {
		return (1 & shiftCount) == 0;
	}
}
