package special;

public class Shuffle {
	public static int[] shuffleArray(int[] array) {
		int index0From = 0;
		int index1From = array.length / 2;
		int index0To= 0;
		int index1To= index0From+1;
		int turnCount = 0;

		while (index0To < array.length || index1To < array.length) {  // interesting �
			int buf = 0;
			// if index0 is not 0, that means it is swapped to the 3rd partition
		if (turnCount % 2 == 1) {
			buf = array[index0To];
			array[index0To] = array[index0From];
			array[index0From] = buf;
		}

	// swap the value of index1 to correct location
	buf = array[index1From];
		
		array[index1To] = array[index1From];
		array[index1From] = buf;

			// because whatever you swapped here is the next lowest from the 2nd partition
			// 2nd partition is after the ones already in order
			if (turnCount %2 == 0) {
				index0From = index1From;
			}
			else {
				index0From = index0From;
			}
		index0To += 2;
		index1From++;
		index1To += 2;
		turnCount++;
		}
		return array;
	}
}
