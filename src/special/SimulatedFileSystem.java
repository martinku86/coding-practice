package special;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class SimulatedFileSystem {

	Directory root;
	public SimulatedFileSystem() {
		root = new Directory(null, "");
	}
	
	public Directory getRoot() {
		return root;
	}
	
	public void startSesstion() {
		new Session(root).start();
	}
	
	interface FileInterface {
		public boolean isDirectory();
		
		public String getName();
		
		public Directory getParent();
		
		public String getAbsolutePath();
	}
	
	abstract class FileGeneric implements FileInterface {
		Directory parent;
		String name;
		
		public FileGeneric(Directory parent, String name) {
			this.parent = parent;
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public boolean isDirectory() {
			return false;
		}
		
		public Directory getParent() {
			return parent;
		}
		
		public String getAbsolutePath() {
			Stack<String> stack = new Stack<String>();
			Directory nextParent = null;
			FileGeneric current = this;
			do {
				stack.push(current.getName());
				nextParent = current.getParent();
				current = (FileGeneric) nextParent;
			} while (nextParent != null);
			
			
			StringBuilder builder = new StringBuilder();
			while (!stack.isEmpty()) {
				builder.append(stack.pop());
				if (!stack.isEmpty())
					builder.append("/");
			}
			return builder.toString();
		}
	}
	
	class Directory extends FileGeneric {
		Map<String, FileGeneric> contentsMap = new HashMap<String, FileGeneric>();
		
		public Directory(Directory parent, String name) {
			super(parent, name);
		}

		public boolean isDirectory() {
			return true;
		}
		
		public void addFile(String name) throws IllegalAccessException {
			addFileInDirectory(new File(this, name));
		}
		
		public void addDirectory(String name) throws IllegalAccessException {
			addFileInDirectory(new Directory(this, name));
		}
		
		public FileGeneric getFile(String filename) throws IllegalAccessException {
			if (contentsMap.containsKey(filename)) {
				return contentsMap.get(filename);
			}
			else {
				throw new IllegalAccessException("file does not exist");
			}
		}
		
		private void addFileInDirectory(FileGeneric file) throws IllegalAccessException {
			if (contentsMap.containsKey(file.getName())) {
				throw new IllegalAccessException("Directory contains file with same name");
			}
			contentsMap.put(file.getName(), file);
		}
	}
	
	public class File extends FileGeneric {

		public File(Directory parent, String name) {
			super(parent, name);
		}
		
	}
	
	public class Session {
		// ls -l (list contents and directory sorted by name)
		// cd .. go to parent
		// pwd list full file name
		// mk "filename" to make file
		// mkdir "dirname" to make directory
		// optional - normal ls, cd "directory", cd "absolutePathName"
		
		Directory current;
		
		public Session(Directory directory) {
			current = directory;
		}
		
		public void start() {
			System.out.println("Welcome to sOS");
			boolean validCommand = true;
			Scanner sc = new Scanner(System.in);
			for (;;) {
				String command = sc.nextLine();
				
				if (command.equalsIgnoreCase("exit")) {
					break;
				}
				
				validCommand = true;
				String[] commandAndArgs = command.split(" ");
				
				switch (commandAndArgs[0]) {
				case "ls":
					executeLS(commandAndArgs);
					break;
				case "cd":
					executeCD(commandAndArgs);
					break;
				case "pwd":
					executePWD(commandAndArgs);
					break;
				case "mk":
					executeMK(commandAndArgs);
					break;
				case "mkdir":
					executeMKDIR(commandAndArgs);
					break;
				case "":
					break;
				default:
					validCommand = false;
					break;
				}
				
				if (!validCommand) {
					System.out.println("Command not recognized");
				}
			}
		}

		private void executeMKDIR(String[] commandAndArgs) {
			if (commandAndArgs.length < 2) {
				System.out.println("please provide a directory name");
				return;
			}
			String filename = commandAndArgs[1];
			if (!validFileName(filename)) {
				return;
			}
			try {
				current.addDirectory(filename);
			} catch (IllegalAccessException e) {
				System.out.println(e.getMessage());
			}
		}

		private void executeMK(String[] commandAndArgs) {
			if (commandAndArgs.length < 2) {
				System.out.println("please provide a file name");
				return;
			}
			String filename = commandAndArgs[1];
			if (!validFileName(filename)) {
				return;
			}
			try {
				current.addFile(filename);
			} catch (IllegalAccessException e) {
				System.out.println(e.getMessage());
			}
		}

		private boolean validFileName(String filename) {
			if (filename == null || filename.equals(""))
				return false;
			
			for (char c : filename.toCharArray()) {
				if (c == '/' ||
					c == '.' ||
					c == ' ')
					return false;
			}
			return true;
		}

		private void executePWD(String[] commandAndArgs) {
			System.out.println(current.getAbsolutePath());
		}

		private void executeCD(String[] commandAndArgs) {
			if (commandAndArgs.length < 2) {
				return;
			}
			if (commandAndArgs[1].equals("..")) {
				if (current.getParent() == null) {
					System.out.println("Already at root node");
					return;
				}
				else {
					current = current.getParent();
				}
				return;
			}
			
			FileGeneric file = null;
			try {
				file = current.getFile(commandAndArgs[1]);
			} catch (IllegalAccessException e) {
				System.out.println(e.getMessage());
				return;
			}
			if (!file.isDirectory()) {
				System.out.println(String.format("%s is not a directory", file.getName()));
			}
			else {
				current = (Directory) file;
			}
		}

		private void executeLS(String[] commandAndArgs) {
			current.contentsMap.keySet().stream().sorted().forEach(s -> System.out.println(s));
		}
	}
}
