package special;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */

public class MergeIntervalBitSet {
	//https://leetcode.com/problems/merge-intervals/
	// merge interval using BitSet
	// intervals are represented by series of TRUE bits
	public List<Interval> merge(List<Interval> intervals) {

        int bitSetSize = 0;
        for(Interval interval : intervals) {
            bitSetSize = (bitSetSize >= interval.end) ? bitSetSize : interval.end;
        }

		BitSet intervalsMask = new BitSet((bitSetSize*2)+1);
		
		for(Interval interval : intervals) {
			intervalsMask.set(interval.start*2, (interval.end*2)+1);
		}
		
		List<Interval> resultList = new LinkedList<Interval>();

		int i = intervalsMask.nextSetBit(0);
		int j = 0;
		while(i >= 0) {
			j = intervalsMask.nextClearBit(i);
			resultList.add(new Interval(i/2, j/2));
			i =  intervalsMask.nextSetBit(j);
		}
		
		return resultList;
	}
	
	//merge if interval are neighbors
	public List<Interval> mergeIncludeNeighbor(List<Interval> intervals) {
		BitSet intervalsMask = new BitSet(Integer.MAX_VALUE);
		
		for(Interval interval : intervals) {
			intervalsMask.set(interval.start, interval.end);
		}
		
		List<Interval> resultList = new LinkedList<Interval>();
		
		int i = intervalsMask.nextSetBit(0);
		int j = 0;
		while(i >= 0) {
			j = intervalsMask.nextClearBit(i);
			resultList.add(new Interval(i, j));
			i =  intervalsMask.nextSetBit(j);
		}
		
		return resultList;
	}
	

}

class Interval {
	int start;
	int end;
	Interval() { start = 0; end = 0; }
	Interval(int s, int e) { start = s; end = e; }
}