package special;

import java.util.Iterator;
import java.util.TreeMap;

public class FindMedian {

	    TreeMap<Integer, Integer> sortedMap;
	    int dataSize;

	    public FindMedian() {
	        sortedMap = new TreeMap<Integer, Integer>();
	        dataSize = 0;
	    }
	    
	    // Adds a number into the data structure.
	    public void addNum(int num) {
	        if (sortedMap.containsKey(num))
	            sortedMap.put(num, sortedMap.get(num)+1);
	        else
	            sortedMap.put(num, 1);
	        
	        dataSize++;
	    }

	    // Returns the median of current data stream
	    public double findMedian() {
	        Iterator<Integer> keyIterator = sortedMap.keySet().iterator();

	        int middleCount = dataSize / 2;
	        int iterationCount = 0;

	        int middleKey = 0;

	        while (iterationCount < middleCount) {
	            middleKey = keyIterator.next();
	            int currentValueCount = sortedMap.get(middleKey);
	            iterationCount = iterationCount + currentValueCount;
	        }

	        Double middleKeyDouble = new Double(middleKey);
	        if (oddDataSize())
	            return middleKeyDouble;
	        
	        if (iterationCount > middleCount)
	            return middleKeyDouble;

	        int nextKey = keyIterator.next();
	        Double nextKeyDouble = new Double(nextKey);
	        
	        return (middleKeyDouble + nextKeyDouble) / 2;
	    }

	    private boolean oddDataSize() {
	        return (dataSize % 2) == 1;
	    }
}
