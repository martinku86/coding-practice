package combination;

import java.util.LinkedList;

/**
 * Given int[] array, and int target, find all combination of elements that sums to target 
 * This question appeared to be mentioned many times as part of Quora interview
 * @author Martin
 *
 */
public class FindNumberOfCombinationToSum {

	public int countCombinationSumToTarget(int[] input, int target) {
		return createCombinationRecursive(input, target, 0, new LinkedList<Integer>(), 0, 0);
	}

	private int createCombinationRecursive(int[] input, int target, int index, LinkedList<Integer> combination, int comboSum, int result) {
		if (comboSum == target && combination.size() != 0)
				result++;
		if (index >= input.length) {
			return result;
		}
		
		for (int i = index; i < input.length; i++) {
			combination.add(input[i]);
			
			result = createCombinationRecursive(input, target, index + 1, combination, comboSum + input[i], result);
			
			combination.removeLast();
		}
		
		return result;
	}
}
