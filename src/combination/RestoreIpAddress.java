package combination;

import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode.com/problems/restore-ip-addresses/
 * 
 * Given a string containing only digits, restore it by returning all possible valid IP address combinations.
 * For example:
 * Given "25525511135",
 * return ["255.255.11.135", "255.255.111.35"]. (Order does not matter)
 * 
 * @author Martin
 *
 */
public class RestoreIpAddress {
static final int DOTS = 3;
    
    public List<String> restoreIpAddresses(String s) {
        List<String> results = new LinkedList<String>();
        
        List<List<Integer>> partitionIndexes = new LinkedList<List<Integer>>();
        
        getIpPartitionsRecursive(s, 0, 0, new LinkedList<Integer>(), partitionIndexes);
        
        StringBuilder builder = new StringBuilder();
        for (List<Integer> partCombo : partitionIndexes) {
            int start = 0;
            for(int index : partCombo) {
                builder.append(s.substring(start, index+1));
                builder.append(".");
                start = index + 1;
            }
            builder.append(s.substring(start, s.length()));
            results.add(builder.toString());
            builder = new StringBuilder();
        }
        
        return results;
    }
    
    private void getIpPartitionsRecursive(String s, int dot, int index, LinkedList<Integer> currentPart, List<List<Integer>> partIndexes) {
        if (dot == DOTS) {
            LinkedList<Integer> clone = new LinkedList<Integer>();
            clone.addAll(currentPart);
            partIndexes.add(clone);
            return;
        }
        
        int remainChars = s.length() - index;
        int maxThreshold = (DOTS - dot + 1) * 3;
        int minThreshold = (DOTS - dot + 1);
        
        if (remainChars < minThreshold || remainChars > maxThreshold)
            return;
        
        maxThreshold -= 3;
        minThreshold -= 1;
        // lower bound should be less than 3, but could be less than 1
        int lower = remainChars - maxThreshold;
        lower = lower <= 1 ? index : index + lower - 1;
        
        // upper should be greater than 1, but could be greater than 3
        int upper = remainChars - minThreshold;
        upper = upper >= 3 ? index + 3 : index + upper;
        
        for (int i = lower; i < upper; i++) {
            currentPart.add(i);
            getIpPartitionsRecursive(s, dot + 1, i+1, currentPart, partIndexes);
            currentPart.removeLast();
        }
    }
}
