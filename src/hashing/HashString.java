package hashing;

public class HashString {
	public static double hasToInt(String s) {
		double h = 0;
		for (int i = 0; i < s.length(); i++) {
			h = h + (s.charAt(i) * Math.pow((double)31, (double)(s.length()-1-i)));
		}
		return h;
	}
}
