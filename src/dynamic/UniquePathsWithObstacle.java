package dynamic;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/*
 * https://leetcode.com/problems/unique-paths-ii/
 * 
 */
public class UniquePathsWithObstacle {
	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int[][] pathsTrackGrid = new int[obstacleGrid.length][];
        int rowCount = 0;
        for (int[] row : obstacleGrid) {
            pathsTrackGrid[rowCount] = new int[row.length];
            rowCount++;
        }
        
        Coordinate departure = new Coordinate(1, 1);
        setGridValueOnCoordinate(pathsTrackGrid, departure, 1);
        
        Queue<Coordinate> pathQ = new LinkedList<Coordinate>();
        pathQ.add(departure);
        
        while(!pathQ.isEmpty()) {
            Coordinate currentCoord = pathQ.poll();
            int coordPathCount = getGridValueOnCoordinate(pathsTrackGrid, currentCoord);
            
            List<Coordinate> validNeighbors = currentCoord.getValidNeighbours(obstacleGrid);
            
            for (Coordinate neighbor : validNeighbors) {
                int neighborPathCount = getGridValueOnCoordinate(pathsTrackGrid, neighbor);
                setGridValueOnCoordinate(pathsTrackGrid, neighbor, coordPathCount + neighborPathCount);
                pathQ.add(neighbor);
            }
        }
        
        Coordinate destination = new Coordinate(obstacleGrid.length, obstacleGrid[obstacleGrid.length-1].length);
        return getGridValueOnCoordinate(pathsTrackGrid, destination);
    }
    
    public int getGridValueOnCoordinate(int[][] grid, Coordinate coord) {
        int y = grid.length - coord.y;
        if (y < 0) return -1;
        int x = grid[y].length - coord.x;
        if (x < 0) return -1;
        
        return  grid[y][x];
    }
    
    public void setGridValueOnCoordinate(int[][] grid, Coordinate coord, int value) {
        int y = grid.length - coord.y;
        int x = grid[y].length - coord.x;
        
        grid[y][x] = value;
    }
    
    class Coordinate {
        int x;
        int y;
        
        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }
        
        public boolean equals(Coordinate other) {
            return (this.x == other.x) && (this.y == other.y);
        }
        
        public List<Coordinate> getValidNeighbours(int[][] grid) {
            // get the valid neighbours for traversal - to right and to top if value not 1
            List<Coordinate> validNeighbors = new LinkedList<Coordinate>();

            Coordinate topNeighbor = new Coordinate(this.x, this.y + 1);
            int validNeighbor = getGridValueOnCoordinate(grid, topNeighbor);
            if (validNeighbor != 1 && validNeighbor != -1)
                validNeighbors.add(topNeighbor);
            
            Coordinate rightNeighbor = new Coordinate(this.x + 1, this.y);
            validNeighbor = getGridValueOnCoordinate(grid, rightNeighbor);
            if (validNeighbor != 1 && validNeighbor != -1)
                validNeighbors.add(rightNeighbor);
            
            return validNeighbors;
        }
    }
}
