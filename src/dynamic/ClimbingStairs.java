package dynamic;

public class ClimbingStairs {
	public static int climbStairs(int n) {
        int[] waysBySteps = new int[n+1];
        
        for (int i = 0; i < n+1; i++) {
            if (i == 0 || i == 1) {
            	waysBySteps[i] = 1;
            	continue;
            }
            
            waysBySteps[i] = waysBySteps[i-1]  + waysBySteps[i-2];
        }

        return waysBySteps[n];
    }
}
