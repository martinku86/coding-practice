package permutation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class WordBreakIITest {

	@Test
	public void staticTest() {
		WordBreakII solution = new WordBreakII();
		
//		String s = "catsanddog";
//		
//		Set<String> wordDict = new HashSet<String>(
//				Arrays.asList("cat", "cats", "dog", "and", "sand"));
		
		String s = "aaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaa";
		
		Set<String> wordDict = new HashSet<String>(
				Arrays.asList("a", "aa", "aaa", "aaaa", "b"));
		
		List<String> result = solution.wordBreak(s, wordDict);
		
		for (String sentence : result) {
			System.out.println(sentence);
		}
	}
}
