package permutation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import permutation.TournamentMatchup.Matchup;

public class TournamentMatchupSchedulerTest {

	@Test
	public void staticMatchupTest() {
		int teamSize = 6; // must be greater than 1
		int[] teams = new int[teamSize];
		
		for (int i = 0; i < teams.length; i++) {
			teams[i] = i;
		}
		
		TournamentMatchup solution = new TournamentMatchup(teams);
		LinkedList<List<Matchup>> matchups = solution.generateMatchUps();
		
		printMatchups(matchups);
		
		Assert.assertTrue(verifyNoMissingOrRepeatMatchup(matchups, teams));
	}

	public void dynamicMatchupTest() {
		
	}
	
	private void printMatchups(LinkedList<List<Matchup>> matchups) {
		int day = 0;
		for (List<Matchup> daySchedule : matchups) {
			System.out.println("Day " + day);
			for (Matchup matchup : daySchedule) {
				System.out.println(matchup.printMatchUpLine());
			}
			day++;
			System.out.println();
		}
	}
	
	private boolean verifyNoMissingOrRepeatMatchup(LinkedList<List<Matchup>> matchUps, int[] teams) {
		HashSet<Matchup> expectedMatchups = generateExpectedMatchups(teams);
		for (List<Matchup> daySchedule : matchUps) {
			for (Matchup matchup : daySchedule) {
				if (!expectedMatchups.contains(matchup))
					return false;
				
				expectedMatchups.remove(matchup);
			}
		}
		return expectedMatchups.isEmpty();
	}
	
	private HashSet<Matchup> generateExpectedMatchups(int[] teams) {
		HashSet<Matchup> expectedMatchUps = new HashSet<Matchup>();
		for (int i = 0; i < teams.length - 1; i++) {
			for (int j = i+1; j < teams.length; j++) {
				expectedMatchUps.add(new Matchup(i, j));
			}
		}
		return expectedMatchUps;
	}
}
