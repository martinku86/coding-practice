package array;

import org.junit.Test;

public class FindMedianMultipleSortedArrayTest {

	@Test
	public void staticArrayTest() {
		int[] nums1 = {1,2,2};
		int[] nums2 = {1,2,3};
//		int[] nums1 = {2};
//		int[] nums2 = {1,3};
		
		FindMedianMultipleSortedArray solution = new FindMedianMultipleSortedArray();
		
		double result = solution.findMedianSortedArrays(nums1, nums2);
		System.out.println(result);
	}
}
