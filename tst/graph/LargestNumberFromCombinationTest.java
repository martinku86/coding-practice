package graph;

import org.junit.Test;

public class LargestNumberFromCombinationTest {

    @Test
    public void testStaticInput() {
        int[] intputArray =
            {1, 101, 21, 121, 123,111};
        
        LargestNumberFromCombination solution = new LargestNumberFromCombination();
        
        String solutionOutput = solution.generateLargestNumber(intputArray);
        
        System.out.println(solutionOutput);
    }
}
