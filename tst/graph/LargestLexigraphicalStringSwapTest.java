package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

import graph.LargestLexigraphicalStringSwap.Indice;

public class LargestLexigraphicalStringSwapTest {

	@Test
	public void testLargestLexigraphicalStringSwap() {
		LargestLexigraphicalStringSwap solution = new LargestLexigraphicalStringSwap();
		
		String input = "abdcelz";
		int[][] indiceArray = new int[][]{{1,4},{3,4},{4,5}};
		
		LinkedList<Indice> indices = generateIndicesFromArrays(indiceArray);
		String resultString = solution.findLargestSwappedString(input, indices);
		
		System.out.println(resultString);
	}

	private LinkedList<Indice> generateIndicesFromArrays(int[][] arrays) {
		LinkedList<Indice> resultList = new LinkedList<Indice>();
		for (int[] indice : arrays) {
			resultList.add(new Indice(indice));
		}
		return resultList;
	}

	public String generateSwapStringUniqueChar(int size) {
		Random random = new Random();
		StringBuilder strBuilder = new StringBuilder();
		
		Set<Character> checkDupSet = new HashSet<Character>();
		for (int i = 0; i < size; i++) {
			char randomChar = (char)(random.nextInt(26) + 'a');
			while (checkDupSet.contains(randomChar))
				randomChar = (char)(random.nextInt(26) + 'a');
			
			strBuilder.append(randomChar);
		}
		return strBuilder.toString();
	}
}
