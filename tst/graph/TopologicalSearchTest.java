package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class TopologicalSearchTest {

	// generate an adjacency matrix
	// run through the solution. expect a list of nodes (linked) by which the dependencies is satisfied
	@Test
	public void topologicalSearchStaticTest() {
		boolean[][] adjMatrix = 
			{
					{false, false, true, true},
					{false, false, false, true},
					{false, true, false, false},
					{false, false, false, false}
			};
		
		TopologicalSearch solution = new TopologicalSearch(adjMatrix);
		LinkedList<Integer> topOrder = solution.search();
		
		printOrder(topOrder);
		
		boolean noDuplicate = verifyNoDuplicate(topOrder);
		Assert.assertTrue(noDuplicate);

		boolean topologicallyCorrect = verifyToplogy(adjMatrix, topOrder);
		Assert.assertTrue(topologicallyCorrect);
	}
	
	private boolean verifyToplogy(boolean[][] adjMatrix, LinkedList<Integer> topOrder) {
		// check if step is valid, non repeating
		Set<Integer> availableStepSet = new HashSet<Integer>();
		Set<Integer> unavailableStepSet = new HashSet<Integer>();
		for (int step : topOrder) {
			if (availableStepSet.isEmpty() || availableStepSet.contains(step)) {
				// search adjMatrix for availableSteps, add to set
				for (int i = 0; i < adjMatrix[step].length; i++) {
					if (adjMatrix[step][i] && !unavailableStepSet.contains(i)) {
						availableStepSet.add(i);
					}
				}
				
				// remove current step from available
				unavailableStepSet.add(step);
				availableStepSet.remove(step);
			}
			else {
				return false;
			}
		}
		return true;
	}
	
	private boolean verifyNoDuplicate(LinkedList<Integer> topOrder) {
		Set<Integer> set = new HashSet<Integer>();
		for (int step : topOrder) {
			if (set.contains(step))
				return false;
			set.add(step);
		}
		return true;
	}
	
	private void printOrder(List<Integer> topOrder) {
		for (int step : topOrder) {
			System.out.print(step + ", ");
		}
	}
}
