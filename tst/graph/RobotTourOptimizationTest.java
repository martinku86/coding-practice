package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class RobotTourOptimizationTest {

	// generate an adjacency matrix
	// run through the solution. expect a list of nodes (linked) by which the dependencies is satisfied
	@Test
	public void topologicalSearchStaticTest() {
		int[][] adjMatrix = 
			{
					{0, 1, 1, 1},
					{1, 0, 3, 2},
					{1, 3, 0, 2},
					{1, 2, 2, 0}
			};
		
		RobotTourOptimization solution = new RobotTourOptimization(adjMatrix);
		LinkedList<Integer> topOrder = solution.getOrder();
		
		printOrder(topOrder);
		
		boolean noDuplicate = verifyNoDuplicate(topOrder);
		Assert.assertTrue(noDuplicate);
	}
	
	private boolean verifyNoDuplicate(LinkedList<Integer> topOrder) {
		Set<Integer> set = new HashSet<Integer>();
		for (int step : topOrder) {
			if (set.contains(step))
				return false;
			set.add(step);
		}
		return true;
	}
	
	private void printOrder(List<Integer> topOrder) {
		for (int step : topOrder) {
			System.out.print(step + ", ");
		}
	}
}
