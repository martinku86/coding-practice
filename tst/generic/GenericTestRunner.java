package generic;
import org.junit.Test;

public class GenericTestRunner {

	@Test
	public void someTest() {
		int[][] inputMatrix =
			{{1,3,5}};
		
		int target = 6;
		
		boolean result = searchMatrix(inputMatrix, target);
		System.out.println(result);
	}
	
	public boolean searchMatrix(int[][] matrix, int target) {
        int rowUpper = matrix.length-1;
        int rowLower = 0;
        int colUpper = (matrix[rowUpper].length) - 1;
        int colLower = 0;
        
        int holder = 0;
        while (rowUpper >= rowLower && colUpper >= colLower) {
            // search upper row then col
            holder = matrix[rowUpper][colLower];
            if (holder == target) return true;
            if (holder > target)
                rowUpper--;
            
            holder = matrix[rowLower][colUpper];
            if (holder == target) return true;
            if (holder > target)
                colUpper--;
            
            if (rowUpper < rowLower || colUpper < colLower)
                return false;
            
            // search lower row then col
            holder = matrix[rowLower][colUpper];
            if (holder == target) return true;
            if (holder < target)
                rowLower++;
            
            holder = matrix[rowUpper][colLower];
            if (holder == target) return true;
            if (holder < target)
                colLower++;
        }
        
        return false;
    }
}
