package string;

import org.junit.Test;

import junit.framework.Assert;

public class WordDistanceFinderTest {
	@Test
	public void staticTest() {
		
		String[] sentence =  {"the", "quick", "brown", "fox", "quick"};
		String word0 = "quick";
		String word1 = "fox";
		WordDistanceFinder solution = new WordDistanceFinder(sentence);
		int distance = solution.findDistance(word0, word1);
		Assert.assertEquals(1, distance);
	}
}
