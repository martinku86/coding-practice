package onehours;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class FindMostProfitableStockTrade {

	public Integer[] findMostProfitableTrade(Integer[] priceByDay) {
		int buyDay = 0;
		int sellDay = 0;

		int currentLowBarDay = 0;
		int currentHighBarDay = 0;
		for(int i = 0; i < priceByDay.length; i++) {
			int currentPrice = priceByDay[i];
			
			if (currentPrice > priceByDay[currentHighBarDay]) {
				currentHighBarDay = i;
			}
			
			if (currentPrice < priceByDay[currentHighBarDay]) {
				if (compareTrade(priceByDay, buyDay, sellDay, currentLowBarDay, currentHighBarDay) > 0) {
					buyDay = currentLowBarDay;
					sellDay = currentHighBarDay;
				}

				if (currentPrice < priceByDay[currentLowBarDay]) {
					currentLowBarDay = i;
					currentHighBarDay = i;
				}
			}
		}

		if (compareTrade(priceByDay, buyDay, sellDay, currentLowBarDay, priceByDay.length-1) > 0) {
			buyDay = currentLowBarDay;
			sellDay = currentHighBarDay;
		}

		return new Integer[]{buyDay, sellDay};
	}
	
	public int compareTrade(Integer[] priceByDay, int currBuyDay, int currSellDay, int newBuyDay, int newSellDay) {
		int currTradeProfit = priceByDay[currSellDay] - priceByDay[currBuyDay];
		int newTradeProfit = priceByDay[newSellDay] - priceByDay[newBuyDay];
		return newTradeProfit - currTradeProfit;
	}
	
	@Test
	public void testFindMostProfitableTradeStatic() {
//		Integer[] priceByDay = {10, 30, 42, 15, 20, 50, 10, 25};
//		Integer[] expectedResult = {0, 5};
		Integer[] priceByDay = {10, 30, 42, 15, 20, 50, 0, 25, 48};
		Integer[] expectedResult = {6, 8};
		
		Integer[] actualResult = findMostProfitableTrade(priceByDay);
		
		printPriceByDayAndResult(priceByDay, actualResult);
		Assert.assertArrayEquals(expectedResult, actualResult);
	}
	
	@Test
	public void testFindMostProfitableTradeDynamic() {
		int size = 20;
		int bound = 30;
		
		Integer[] priceByDay = generatePriceByDay(size, bound);
		Integer[] actualResult = findMostProfitableTrade(priceByDay);
		
		printPriceByDayAndResult(priceByDay, actualResult);
	}
	
	private Integer[] generatePriceByDay(int size, int bound) {
		ArrayList<Integer> resultList = new ArrayList<Integer>(size);
		
		Random random = new Random();
		for (int i = 0; i < size; i++) {
			resultList.add(random.nextInt(bound));
		}
		
		Integer[] resultArray = new Integer[size];
		return resultList.toArray(resultArray);
	}
	
	private void printPriceByDayAndResult(Integer[] priceByDay, Integer[] results) {
		System.out.print("Price By Day: ");
		for(Integer price : priceByDay)
			System.out.print(String.format("%d ", price));
		System.out.println();
		
		System.out.print("Result: ");
		for(Integer result : results)
			System.out.print(String.format("%d ", result));
		System.out.println();
		
	}
}
