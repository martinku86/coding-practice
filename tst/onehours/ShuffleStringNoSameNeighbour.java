package onehours;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ShuffleStringNoSameNeighbour {

	@Test
	public void testStatic() {
		String string = "AAABC";
		String result = shuffleStringNoSameNeighbour(string);
		
		System.out.println(result);
	}

	public String shuffleStringNoSameNeighbour(String string) {
		char[] resultStringArray = new char[string.length()];
		
		Map<Character, Integer> mapLetterByCount = new HashMap<Character, Integer>();
		for (int i = 0; i < string.length(); i++) {
			Character c = string.charAt(i);
			
			if (mapLetterByCount.containsKey(c))
				mapLetterByCount.put(c, mapLetterByCount.get(c)+1);
			else
				mapLetterByCount.put(c, 1);
		}
		
		fillArrayEveryOtherSpace(resultStringArray, mapLetterByCount);
		
		fillArrayRemainingSpace(resultStringArray, mapLetterByCount);
		
		return new String(resultStringArray);
	}

	private void fillArrayEveryOtherSpace(char[] charArray, Map<Character, Integer> map) {
		Character currentChar = findNextHighestChar(map);
		for (int i = 0; i < charArray.length; i = i + 2) {
			charArray[i] = currentChar;
			int currentCharCount = map.get(currentChar) - 1;
			
			if (currentCharCount < 1) {
				map.remove(currentChar);
				currentChar = findNextHighestChar(map);
			}
			else {
				map.put(currentChar, currentCharCount);
			}
		}
	}

	private void fillArrayRemainingSpace(char[] charArray, Map<Character, Integer> map) {
		int index = 1;
		
		for (Character k : map.keySet()) {
			for (int i = 0; i < map.get(k); i++) {
				charArray[index] = k;
				index = index + 2;
			}
		}
	}

	private Character findNextHighestChar(Map<Character, Integer> mapLetterByCount) {
		Character highestCountCharacter = null;
		int highestCount = 0;
		for (Character c : mapLetterByCount.keySet()) {
			if (highestCount < mapLetterByCount.get(c)) {
				highestCount = mapLetterByCount.get(c);
				highestCountCharacter = c;
			}
		}
		return highestCountCharacter;
	}
}
