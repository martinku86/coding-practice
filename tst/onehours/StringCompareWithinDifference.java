package onehours;

import org.junit.Test;

import org.junit.Assert;

public class StringCompareWithinDifference {

	public boolean ifArrayContains1CharDiffWord(String originalString, String[] stringArray) {
		int allowableDifference = 1;
		for(String compareTarget : stringArray) {
			if (new DifferenceTracker(originalString, compareTarget, allowableDifference).withinAllowableDifference())
			return true;
		}
		return false;
	}

	class DifferenceTracker {
		int allowableDifference;
		String original;
		String compareTarget;

		public DifferenceTracker(String original, String compareTarget, int allowableDifference) {
			this.original = original;
			this.compareTarget = compareTarget;
			this.allowableDifference = allowableDifference;
		}

		public boolean withinAllowableDifference() {
			int compareLength = original.length() - compareTarget.length();

			if (Math.abs(compareLength) > 1)
				return false;

			boolean diffLength = compareLength != 0;

			String shorter = compareLength < 0 ? original : compareTarget;
			String longer = compareLength < 0 ? compareTarget : original;
			
			int longIndexMod = 0;
			
			compareLength = (compareLength < 0) ? original.length() : compareTarget.length();

			for(int i = 0; i < compareLength; i++) {
				if (diffLength) {
					if (shorter.charAt(i) != longer.charAt(i + longIndexMod)) {
						allowableDifference--;
						i--;
						longIndexMod++;
					}
				}
				else {
					if (original.charAt(i) != compareTarget.charAt(i))
						allowableDifference--;
				}
				
				if (allowableDifference < 0)
					return false;
			}
			return true;
		}
	}
	
	@Test
	public void testStringCompareWithDifference() {
		String originalString = "banana";
		String[] compareList = {
				"anana"
		};
		
		boolean expectedResult = true;
		
		boolean result = ifArrayContains1CharDiffWord(originalString, compareList);
		Assert.assertEquals(expectedResult, result);
	}
}
