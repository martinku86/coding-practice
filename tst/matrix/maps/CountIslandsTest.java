package matrix.maps;

import org.junit.Test;

public class CountIslandsTest {

	@Test
	public void testCountIsland() {
		final int[][] map = {
				{0, 0, 1, 0},
				{0, 1, 1, 0},
				{1, 0, 0, 1},
				{1, 1, 1, 1}
		};
		
		FindBiggestIslandSize solution = new FindBiggestIslandSize();
		System.out.println(solution.solution(map));
	}
}
