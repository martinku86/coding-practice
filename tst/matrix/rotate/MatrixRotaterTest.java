package matrix.rotate;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class MatrixRotaterTest {

	@Test
	public void testRotateSquareMatrix() {
		Random random = new Random();
		int[][] squareMatrix = generateSquareMatrix(random.nextInt(8));
		printMatrix(squareMatrix);
		System.out.println();

		MatrixRotater matrixRotater = new MatrixRotater(squareMatrix);
		int[][] rotatedMatrix = matrixRotater.rotateClockWise();
		printMatrix(rotatedMatrix);
		System.out.println();

		Assert.assertTrue(true);
	}

	private int[][] generateSquareMatrix(int length) {
		int counter = 0;
		int[][] squareMatrix = new int[length][];
		for (int i = 0; i < squareMatrix.length; i++) {
			squareMatrix[i] = new int[length];
			for (int j = 0; j < squareMatrix[i].length; j++) {
				squareMatrix[i][j] = counter;
				counter++;
			}
		}
		return squareMatrix;
	}

	private void printMatrix(int[][] matrix) {
		for (int[] row : matrix) {
			for (int element : row) {
				System.out.print(String.format("%d\t", element));
			}
			System.out.println();
		}
	}
}
