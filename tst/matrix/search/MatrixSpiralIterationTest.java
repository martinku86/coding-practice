package matrix.search;

import java.util.Random;

import org.junit.Test;

import junit.framework.Assert;
import matrix.search.MatrixSpiralIteration.CornerSpiralNavigator;

public class MatrixSpiralIterationTest {

	@Test
	public void spiralIterationTest() {
		MatrixSpiralIteration solution = new MatrixSpiralIteration();
		int[][] matrix = solution.generateMatrix(3);

		printMatrix(matrix);
	}
	
	private void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[");
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j]);
				System.out.print(", ");
			}
			System.out.print("]\n");
		}
	}
	
	// length test
	@Test
	public void spiralNavigatorLength1Test() {
		final int n = 1;
		Assert.assertEquals(1, navigatorLengthTest(n));
	}
	
	@Test
	public void spiralNavigatorLength2Test() {
		final int n = 2;
		Assert.assertEquals(n * n , navigatorLengthTest(n));
	}
	
	@Test
	public void spiralNavigatorLength3Test() {
		final int n = 3;
		Assert.assertEquals(n * n, navigatorLengthTest(n));
	}
	
	@Test
	public void spiralNavigatorLengthRand10Test() {
		int n = 4;
		final int random = new Random().nextInt(6);
		n += random;
		Assert.assertEquals(n * n, navigatorLengthTest(n));
	}
	
	private int navigatorLengthTest(int n) {
		MatrixSpiralIteration iterator = new MatrixSpiralIteration();
		CornerSpiralNavigator navigator = iterator.new CornerSpiralNavigator(n);
		
		int count = 0;
		int navigatorIndicator = 0;
		do {
			navigatorIndicator = navigator.setValueAndNavigateNext(count);
			count++;
		} while (navigatorIndicator > 0);
		
		printMatrix(navigator.matrix);
		
		return count;
	}
	
	// corner test. need both odd and even edges
	@Test
	public void spiralNavigatorCornerTest() {
		final int n = 3;
		
		MatrixSpiralIteration iterator = new MatrixSpiralIteration();
		
		CornerSpiralNavigator navigator = iterator.new CornerSpiralNavigator(n);
		
		int count = 1;
		while (navigator.setValueAndNavigateNext(count) > 0) {
//			if (count % n == 0) {
				int cornerIndex = navigator.cornerIndex;
				int cornerY = navigator.corners[navigator.cornerIndex].y;
				int cornerX = navigator.corners[navigator.cornerIndex].x;
				int cornerVal = navigator.matrix[cornerY][cornerX];
				System.out.println(String.format("CornerIndex=%d, CornerCoord={%d, %d}, CornerValue=%d",
													cornerIndex,
													cornerY,
													cornerX,
													cornerVal));
//			}
			count++;
		}
	}
}
