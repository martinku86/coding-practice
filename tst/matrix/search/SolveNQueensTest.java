package matrix.search;

import java.util.List;

import org.junit.Test;

public class SolveNQueensTest {

	@Test
	public void staticTest() {
		final int n = 5;
		
		SolveNQueens solution = new SolveNQueens();
		List<List<String>> results = solution.solveNQueens(n);
		for (List<String> result : results) {
			System.out.println("Result Board:");
			
			for (String line : result) {
				System.out.println(line);
			}
			
			System.out.println();
		}
	}
}
