package combination;

import java.util.List;

import org.junit.Test;

public class RestoreIpAddressTest {

	@Test
	public void restoreIpAddressStatic() {
		String s = "0000";
		
		RestoreIpAddress solution = new RestoreIpAddress();
		
		List<String> ipAddressList = solution.restoreIpAddresses(s);
		
		for (String address : ipAddressList) {
			System.out.println(address);
		}
	}
}
