package stack;

import java.util.Iterator;
import java.util.Random;
import java.util.Stack;

import org.junit.Test;

import org.junit.Assert;

public class StackSortingTest {

	@Test
	public void testStackSorting() {
		int size = 15;
		Stack<Integer> stack = generateRandomStack(size);
		printStack(stack);
		
		Stack<Integer> sortedStack = StackSorting.sortStack(stack);
		printStack(sortedStack);
		
		Assert.assertEquals(size, sortedStack.size());
		Assert.assertTrue(checkAscOrderAndRestack(stack));
	}
	
	private Stack<Integer> generateRandomStack(int size) {
		Random random = new Random();
		Stack<Integer> randomStack = new Stack<Integer>();
		for (int i = 0; i < size; i++) {
			randomStack.push(random.nextInt(size));
		}
		return randomStack;
	}
	
	private void printStack(Stack<Integer> stack) {
		Iterator<Integer> iter = stack.iterator();

		boolean boolFlag = true;
		while (iter.hasNext()) {
			Integer intBuf = iter.next();
			System.out.print(String.format("%d ", intBuf));
		}
		System.out.println("");
	}
	
	private boolean checkAscOrderAndRestack(Stack<Integer> stack) {
		Iterator<Integer> iter = stack.iterator();

		boolean boolFlag = true;
		while (iter.hasNext()) {
			Integer intBuf = iter.next();
			if (intBuf > stack.peek()) {
				boolFlag = false;
				break;
			}
		}
		return boolFlag;
	}
}
