package stack;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;

import org.junit.Test;

import org.junit.Assert;

public class TowerOfHanoiSolutionTest {

	@Test
	public void testEmptyTower() {
		System.out.println("Start testEmptyTower");
		Stack<Integer>[] towers = generateTowers(0);
		printTowers(towers);
		
		System.out.println("Moving Rings");
		TowerOfHanoiSolution.solveTowerOfHanoidFirstToThird(towers);
		
		printTowers(towers);
		Assert.assertTrue(towers[0].empty());
		Assert.assertTrue(towers[1].empty());
		Assert.assertTrue(towers[2].empty());
		System.out.println("End testEmptyTower");
	}
	
	@Test
	public void testOneRingTower() {
		System.out.println("Start testOneRingTower");
		Stack<Integer>[] towers = generateTowers(1);
		printTowers(towers);
		
		System.out.println("Moving Rings");
		TowerOfHanoiSolution.solveTowerOfHanoidFirstToThird(towers);

		printTowers(towers);
		Assert.assertTrue(towers[0].empty());
		Assert.assertTrue(towers[1].empty());
		Assert.assertEquals(new Integer(1), towers[2].peek());
		System.out.println("End testOneRingTower");
	}
	
	@Test
	public void test2To12Tower() {
		System.out.println("Start test2To12Tower");

		Random random = new Random();
		Integer size = random.nextInt(10) + 2;
		
		Stack<Integer>[] towers = generateTowers(size);
		printTowers(towers);
		
		System.out.println("Moving Rings");
		TowerOfHanoiSolution.solveTowerOfHanoidFirstToThird(towers);

		printTowers(towers);
		Assert.assertTrue(towers[0].empty());
		Assert.assertTrue(towers[1].empty());
		System.out.println("End test2To12Tower");
	}

	private void printTowers(Stack<Integer>[] towers) {
		for(Stack<Integer> tower : towers) {
			Stack<Integer> holderStack = new Stack<Integer>();
			while(!tower.empty()) {
				System.out.print(tower.pop());
				System.out.print(" ");
			}
			
			System.out.print(" - Bottom");
			System.out.print("\n");
			
			while(!holderStack.empty()) {
				tower.push(holderStack.pop());
			}
			
		}
		System.out.println();
	}
	
	private Stack<Integer>[] generateTowers(int towerSize) {

		Stack<Integer>[] towers = new Stack[3];
		for (int i = 0; i < towers.length; i++) {
			towers[i] = new Stack<Integer>();
		}
		
		towerSize--;
		while (towerSize >= 0) {
			towers[0].push(towerSize);
			towerSize--;
		}
		return towers;
	}
}
