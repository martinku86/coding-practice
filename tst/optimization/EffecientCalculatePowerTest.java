package optimization;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class EffecientCalculatePowerTest {

    @Test
    public void testCacluatePowerRandom() {
        Random rn = new Random();
        int randomBase = rn.nextInt(10) + 1;
        int randomPower = rn.nextInt(10);
        
        int expectedAnswer = (int) Math.pow(randomBase, randomPower);
        
        int actualAnswer = EffecientCalculatePower.power(randomBase, randomPower);
        
        System.out.println(String.format("base=%d power=%d answer=%d", randomBase, randomPower, actualAnswer));
        
        Assert.assertEquals(expectedAnswer, actualAnswer);
    }
}
