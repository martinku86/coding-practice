package optimization;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

import junit.framework.Assert;
import optimization.FourSum.Pair;

public class FourSumTest {
		// unit tests Pair
	
		FourSumTestEnvironment testEnv;
		int[] inputArray;
		
		@Test
		public void pairGetSumTest() throws IllegalAccessException {
			testEnv = new FourSumTestEnvironment();
			testEnv.getInputArray();
			Pair pair = testEnv.createPairFromIndex(testEnv.randomIndexNoRepeat(), testEnv.randomIndexNoRepeat());
			inputArray = testEnv.getInputArray();
			int[] pairIndexes = pair.getIndexes();
			int expectedSum = inputArray[pairIndexes[0]] + inputArray[pairIndexes[1]]; 
			Assert.assertEquals(expectedSum, pair.getSum());
		}
	
		int indexA;
		int indexB;
		int indexC;
		Set<Pair> dset;
		Pair firstPair;
		Pair secondPair;
		
		boolean pairDsetAddTested = false;

		@Test
		public void staticTest() {
			int[] inputArray = new int[]{1,0,-1,0,-2,2};
			int target = 0;
			
			// print input
			System.out.println(String.format("target=%d", target));
			System.out.println("InputArray=");
			for (int i : inputArray)
				System.out.print(String.format("%d, ", i));
			System.out.println();
			
			testEnv = new FourSumTestEnvironment(inputArray);
			
			// print result
			System.out.println(testEnv.getFourSum(target));
		}

		@Test
		public void pairDsetAdd() throws IllegalAccessException {
			testEnv = new FourSumTestEnvironment();
			testEnv.initialize();
			dset = testEnv.createPairDisjointSet();
			
			indexA = testEnv.randomIndexNoRepeat();
			indexB = testEnv.randomIndexNoRepeat();
			firstPair = testEnv.createPairFromIndex(indexA, indexB);
			
			dset.add(firstPair);
			
			indexC = testEnv.randomIndexNoRepeat();
			secondPair = testEnv.createPairFromIndex(indexB, indexC);
			
			dset.add(secondPair);
			
			Assert.assertNotNull(dset);
			pairDsetAddTested = true;

		}
		
		@Test
		public void pairDsetGetSet() throws IllegalAccessException {
			pairDsetAdd();
			Assert.assertTrue(pairDsetAddTested);
			Assert.assertNotNull(dset);
			
			Assert.assertTrue(dset.contains(firstPair));
			Assert.assertTrue(dset.contains(secondPair));
		}

		@Test
		public void pairDsetContains() throws IllegalAccessException {
			pairDsetAdd();
			Assert.assertTrue(pairDsetAddTested);
			Assert.assertTrue(dset.contains(firstPair));
			Assert.assertTrue(dset.contains(secondPair));
		}
		
		// basic positive test - 4 elements that sums to target
		@Test
		public void fourSumHappyCase() throws IllegalAccessException {
			testEnv = new FourSumTestEnvironment();
			
			int[] inputArray = testEnv.getInputArray();

			int[] indexes = new int[4];
			for (int i = 0; i < indexes.length; i++) {
				indexes[i] = testEnv.randomIndexNoRepeat();
			}
			
			int target = 0;
			HashMap<Integer, Integer> expectedPartialSolution = new HashMap<Integer, Integer>();
			for (int index : indexes) {
				int value = inputArray[index];
				target += value;
				if (!expectedPartialSolution.containsKey(value))
					expectedPartialSolution.put(value, 0);
				expectedPartialSolution.put(value, expectedPartialSolution.get(value) + 1);
			}
			
			List<List<Integer>> result = testEnv.getFourSum(target);
			Assert.assertNotNull(result);
			
			boolean hasExpectedPartialSolution = false;
			for (List<Integer> partialSolution : result) {
				Assert.assertNotNull(partialSolution);
				Assert.assertFalse(partialSolution.isEmpty());
				
				int solutionSum = 0;
				
				HashMap<Integer, Integer> partialSolutionMap = new HashMap<Integer, Integer>();
				for (int element : partialSolution) {
					solutionSum += element;
					if (!partialSolutionMap.containsKey(element))
						partialSolutionMap.put(element, 0);
					partialSolutionMap.put(element, partialSolutionMap.get(element) + 1);
				}
				
				Assert.assertEquals(target, solutionSum);
				
				if (expectedPartialSolution.equals(partialSolutionMap))
					hasExpectedPartialSolution = true;
			}
			
			Assert.assertTrue(hasExpectedPartialSolution);
		}

	class FourSumTestEnvironment {
		int[] inputArray;
		Iterator<Integer> nextRandomIndexIterator;
		List<Integer> randomIndexList;
		FourSum solution;
		
		public FourSumTestEnvironment() {
			this.inputArray = generateRandomIntArray();
			initialize();
		}

		public FourSumTestEnvironment(int[] inputArray) {
			this.inputArray = inputArray;
			initialize();
		}

		public void initialize() {
			solution = new FourSum();
			Integer[] indexArray = new Integer[inputArray.length];
			for (int i = 0; i < inputArray.length; i++) {
				indexArray[i] = i;
			}
			randomIndexList = Arrays.asList(indexArray);
			resetRandomSelection();
		}
		
		public List<List<Integer>> getFourSum(int target) {
			return solution.fourSum(getInputArray(), target);
		}
		
		public int randomIndexNoRepeat() throws IllegalAccessException {
			if (nextRandomIndexIterator.hasNext()) {
				return nextRandomIndexIterator.next();
			}
			throw new IllegalAccessException("No more indexes, you can call resetRandomIndexSelection()");
		}
		
		public void resetRandomSelection() {
			Collections.shuffle(randomIndexList);
			nextRandomIndexIterator = randomIndexList.iterator();
		}
		
		public int[] getInputArray() {
			return inputArray;
		}
		
		public Pair createPairFromIndex(int indexA, int indexB) {
			return solution.new Pair(getInputArray(), indexA, indexB);
		}
		
		public Set<Pair> createPairDisjointSet() {
			return new HashSet<Pair>();
		}
		
		private int[] generateRandomIntArray() {
			// must be at least 4 long
			Random rn = new Random();
			int arraySize = rn.nextInt(10) + FourSum.MIN_ARRAY_SIZE;
			int[] array = new int[arraySize];
			for (int i = 0; i < array.length; i++) {
				array[i] = generateRandomIntPlusMinusHundred();
			}
			return array;
		}
		
		private int generateRandomIntPlusMinusHundred() {
			Random rn = new Random();
			return rn.nextInt(200) - 100;
		}
	}
}
