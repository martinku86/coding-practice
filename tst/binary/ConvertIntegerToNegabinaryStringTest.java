package binary;

import org.junit.Test;

public class ConvertIntegerToNegabinaryStringTest {

	@Test
	public void staticTest() {
		int limit = 15;
		for (int i = 0; i < limit; i++) {
			String format = "Decimal %d, Negabinary %s";
			String s = ConvertIntegerToNegabinaryString.solution(i);
			System.out.println(String.format(format, i, s));
		}
	}
}
