package binary;

import org.junit.Test;

public class InsertOneBitToAnotherTest {

	@Test
	public void staticTest() {
		int result = InsertOneBitToAnother.solution("1000000000", "10011", 2, 6);
		System.out.println(Integer.toString(result, 2));
	}
}
