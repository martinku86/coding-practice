package special;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

import org.junit.Test;

public class FindMedianTest {

	@Test
	public void testFindMedianStatic() {
		ArrayList<Integer> dataList = new ArrayList<Integer>(
				Arrays.asList(0, 5)
				);
		
		FindMedian solution = new FindMedian();
		dataList.forEach(i -> solution.addNum(i));
		
		System.out.println("And the median is " + Double.toString(solution.findMedian()));
	}

	@Test
	public void testFindMedianDynamic() {
		LinkedList<Integer> dataList = generateData(10, 3);
		
		FindMedian solution = new FindMedian();
		dataList.forEach(i -> solution.addNum(i));
		
		System.out.println("And the median is " + Double.toString(solution.findMedian()));
	}

	private LinkedList<Integer> generateData(int dataSize, int bound) {
		Random random = new Random();
		
		LinkedList<Integer> returnList = new LinkedList<Integer>();
		for (int i = 0; i < dataSize; i++) {
			int number = random.nextInt(bound);
			returnList.add(number);
			
			System.out.print(String.format("%d ", number));
		}
		
		System.out.println();
		return returnList;
	}
}