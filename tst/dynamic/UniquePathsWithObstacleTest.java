package dynamic;

import org.junit.Test;

public class UniquePathsWithObstacleTest {

	@Test
	public void test() {
		int[][] input =
			{
					{0}
			};
		
		UniquePathsWithObstacle solution = new UniquePathsWithObstacle();
		int paths = solution.uniquePathsWithObstacles(input);
		System.out.println(paths);
	}
}
